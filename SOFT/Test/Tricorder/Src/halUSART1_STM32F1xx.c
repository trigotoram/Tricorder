#include "halUSART.h"
#include "board.h"


#ifdef HAL_USART1

#if USART1_TBUF_SIZE < 2
    #error "USART1_TBUF_SIZE is too small.  It must be larger than 1."
#elif ((USART1_TBUF_SIZE & (USART1_TBUF_SIZE - 1)) != 0)
    #error "USART1_TBUF_SIZE must be a power of 2."
#endif

#if USART1_RBUF_SIZE < 2
    #error "USART1_RBUF_SIZE is too small.  It must be larger than 1."
#elif ((USART1_RBUF_SIZE & (USART1_RBUF_SIZE - 1)) != 0)
    #error "USART1_RBUF_SIZE must be a power of 2."
#endif

#ifndef SYS_FREQ
    #error "SYS_FREQ not defined!"
#endif

static struct tbuf_1_ {
    U32 in; // Next In Index
    U32 out; // Next Out Index
    U8 buf [USART1_TBUF_SIZE]; // Buffer
} tbuf_1;

static struct rbuf_1_ {
    U32 in; // Next In Index
    U32 out; // Next Out Index
    U8 buf [USART1_RBUF_SIZE]; // Buffer
} rbuf_1;

//static struct buf_st_1 rbuf_1 = { 0, 0, };
#define SIO_RBUFLEN_1 ((U16)(rbuf_1.in - rbuf_1.out)) 

//static struct buf_st_1 tbuf_1 = { 0, 0, };
#define SIO_TBUFLEN_1 ((U16)(tbuf_1.in - tbuf_1.out))

static U32 tx_restart_1 = 1; // NZ if TX restart is required


static U32 USART1_RecBytes = 0;
U32    halUSART1_getRecBytes (void)
{
    return USART1_RecBytes;
}


static U32 USART1_SendBytes = 0;
U32    halUSART1_getSendBytes (void)
{
    return USART1_SendBytes;
}


static U32 USART1_errCount = 0; // ������ ������� ������
U32    halUSART1_getErrors (void)
{
    return USART1_errCount;
}


void halUSART1_flush (void)
{
    tbuf_1.in = 0; // Clear com buffer indexes
    tbuf_1.out = 0;
    tx_restart_1 = 1;

    rbuf_1.in = 0;
    rbuf_1.out = 0;
}


//------------------------------------------------------------------------------
// �������� �����/��
//------------------------------------------------------------------------------
MSG halUSART1_sndS (U8 dat)
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct tbuf_1_ *p = &tbuf_1;
    
    if (SIO_TBUFLEN_1 < USART1_TBUF_SIZE) // If the buffer is full, return an error value
    {
        p->buf [p->in & (USART1_TBUF_SIZE - 1)] = dat; // Add data to the transmit buffer.
        p->in++;
        if (tx_restart_1) // If transmit interrupt is disabled, enable it
        {
            tx_restart_1 = 0;
            resp = FUNCTION_RETURN_OK;
            USART1->CR1 |= USART_CR1_TXEIE; //USART_FLAG_TXE; // enable TX interrupt
        }
    }

    return resp;
}


MSG halUSART1_sndM (U8 *pDat, U16 dataSize) 
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct tbuf_1_ *p = &tbuf_1;
    U16 i;
    
    if ((0 != dataSize) && (USART1_TBUF_SIZE >= dataSize))
    { // ��� �� �� ���������, ���. �� ���
        if ((USART1_TBUF_SIZE - SIO_TBUFLEN_1) < dataSize) // 
        { return resp; }
        if (SIO_TBUFLEN_1 >= USART1_TBUF_SIZE) // If the buffer is full, return an error value
        { return resp; }
        for (i = 0; i < dataSize; i++) // Add data to the transmit buffer.
        {
            p->buf [p->in & (USART1_TBUF_SIZE - 1)] = pDat[i]; 
            p->in++;
        }
        if (0 != tx_restart_1) // If transmit interrupt is disabled, enable it 
        {                 
            tx_restart_1 = 0;
            USART1->CR1 |= USART_CR1_TXEIE; //USART_FLAG_TXE; // enable TX interrupt
        }
        resp = FUNCTION_RETURN_OK;
    }

    return resp;
}


//------------------------------------------------------------------------------
// ����� �����/��
//------------------------------------------------------------------------------
MSG halUSART1_rcvS (U8 *pDat)
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct rbuf_1_ *p = &rbuf_1;

    if (SIO_RBUFLEN_1 != 0)
    {
        *pDat =  (p->buf [(p->out) & (USART1_RBUF_SIZE - 1)]);
        p->out++;
        resp = FUNCTION_RETURN_OK;
    }
    
    return resp;
}


MSG halUSART1_rcvM (U8 *pDat, U16 dataSize)
{
   MSG resp = FUNCTION_RETURN_ERROR;
    struct rbuf_1_ *p = &rbuf_1;
    U16 i;

    if (0 < dataSize)
    {
        if ((0 < SIO_RBUFLEN_1) && (SIO_RBUFLEN_1 >= dataSize))
         {
            for (i = 0; i < dataSize; i++)
            {
                pDat[i] = (p->buf [(p->out) & (USART1_RBUF_SIZE - 1)]);
                p->out++;
            }
            resp = FUNCTION_RETURN_OK;
        }
    }
    
    return resp;
}


void halUSART1_setBaud (U32 baud)
{
    USART1->BRR = (SYS_FREQ) / baud; // APB2 = 72MHz
}


void halUSART1_init (U32 baud)
{
    RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_USART1EN);

  //���������������� PORTA.9 ��� TX
    GPIOA->CRH &= ~GPIO_CRH_MODE9;   //�������� ������� MODE
    GPIOA->CRH &= ~GPIO_CRH_CNF9;    //�������� ������� CNF
    GPIOA->CRH |=  GPIO_CRH_MODE9;   //�����, 50MHz
    GPIOA->CRH |=  GPIO_CRH_CNF9_1;  //�������������� �������, �����������

    //���������������� GPIOA.10 ��� RX 
    GPIOA->CRH &= ~GPIO_CRH_MODE10;  //�������� ������� MODE
    GPIOA->CRH &= ~GPIO_CRH_CNF10;   //�������� ������� CNF
    GPIOA->CRH |=  GPIO_CRH_CNF10_1; //���������� ����, �������� � �����
    GPIOA->BSRR =  GPIO_BSRR_BS10;   //�������� ������������� ��������
    
    //������� ������ ������
    if ((110 > baud) && (921600 < baud))
    {
        baud = 9600;
    }
    halUSART1_setBaud (baud);

    USART1->CR1 &= ~USART_CR1_M;        //8 ��� ������
    USART1->CR2 &= ~USART_CR2_STOP;     //���������� ����-�����: 1
    //���������� �������
    USART1->CR1 |= USART_CR1_UE;        //��������� ������ USART1
    USART1->CR1 |= USART_CR1_TE;        //��������� �����������
    USART1->CR1 |= USART_CR1_RE;        //��������� ���������


    NVIC_EnableIRQ (USART1_IRQn);
    NVIC_SetPriority (USART1_IRQn, 6);

    USART1_SendBytes = 0;
    USART1_RecBytes = 0;
    USART1_errCount = 0;
    
    USART1->CR1 |= USART_CR1_RXNEIE;
    
    halUSART1_flush ();
}


void halUSART1_deinit (void)
{
    USART1->CR1 &= ~USART_CR1_UE;
    GPIOA->CRH &= ~(GPIO_CRH_MODE9 | GPIO_CRH_MODE10);
    GPIOA->CRH &= ~(GPIO_CRH_CNF9 | GPIO_CRH_CNF10);
}


/**
 * Common interrupt  
 */
void USART1_IRQHandler (void)
{
    volatile U16 fStatus;
    struct tbuf_1_ *p_tx;
    struct rbuf_1_ *p_rx;
    
    fStatus = USART1->SR;
    if ((USART_SR_TXE & fStatus) != 0) // ������������� �� ���������� ������� ���������� ��������
    {
        USART1_LED_INV; // ����������� ���������
        USART1->SR &= ~USART_SR_TXE; // clear interrupt
        p_tx = &tbuf_1;
        if (p_tx->in != p_tx->out)
        {
            USART1->DR = (p_tx->buf [p_tx->out & (USART1_TBUF_SIZE - 1)] & 0x1FF);
            p_tx->out++;
            tx_restart_1 = 0;
            USART1_SendBytes++;
        }
        else
        {
            tx_restart_1 = 1;
            USART1->CR1 &= ~USART_CR1_TXEIE; // disable TX interrupt if nothing to send
        }
    }
    
    if ((USART_SR_RXNE & fStatus) != 0)
    {
        USART1_LED_INV; // ����������� ���������
        USART1->SR &= ~USART_SR_RXNE;
        p_rx = &rbuf_1;
        if (((p_rx->in - p_rx->out) & ~(USART1_RBUF_SIZE - 1)) == 0)
        {
            p_rx->buf [p_rx->in & (USART1_RBUF_SIZE - 1)] = (USART1->DR & 0x1FF);
            p_rx->in++;
            USART1_RecBytes++;
        } else { // ���� ����� ��������, �� �������� ���������� �� ��������� �����
            
        }
    }
    
    if ((USART_SR_NE & fStatus)  //!<Noise Error Flag 
        || (USART_SR_FE & fStatus)  //!<Framing Error 
        || (USART_SR_PE & fStatus)  //!<Parity Error 
        || (USART_SR_ORE & fStatus)) // !<OverRun Error 
    {
    //USART_ClearITPendingBit(USARTX, USART_IT_ORE);
        USART1_errCount++;
    }
}

#endif