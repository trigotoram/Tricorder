// ������� - http://www.mdfly.com/newmdfly/products/LPC2148TFTGameboard/SPFD54124B.pdf

/// https://github.com/gresolio/N3310Lib/blob/master/n3310.c
// http://we.easyelectronics.ru/lcd_gfx/vyvod-teksta-na-displey-nokia-1616.html
// http://we.easyelectronics.ru/lcd_gfx/vyvod-kartinok-na-displey-nokia-1616.html
// http://we.easyelectronics.ru/lcd_gfx/shrifty-bolshe-horoshih-i-raznyh.html
// http://we.easyelectronics.ru/lcd_gfx/podklyuchenie-displeya-nokia-1616-na-primere-lpc1343.html
// !!!!!!!!!
// http://radiokot.ru/articles/53/
// http://we.easyelectronics.ru/OlegG/ispolzovanie-usart-stm32-dlya-upravleniya-lcd-nokia-1202.html
//!!!!!!!!!!!


#include "halPaint.h"
#include "board.h"
#include "modPaint.h"

#if (LCD_NOKIA1616)

#define rv

#define SPFD54124B_SEND_CMD		0x0000
#define SPFD54124B_SEND_DATA	0x0100

#define SPFD54124B_CMD_NOP		rv(0x00)
#define SPFD54124B_CMD_SLPOUT	rv(0x11)
#define SPFD54124B_CMD_NORON	rv(0x13)

#define SPFD54124B_CMD_INVOFF	rv(0x20)
#define SPFD54124B_CMD_INVON	rv(0x21)
#define SPFD54124B_CMD_DISPON	rv(0x29)
#define SPFD54124B_CMD_CASET	rv(0x2A)
#define SPFD54124B_CMD_RASET	rv(0x2B)
#define SPFD54124B_CMD_RAMWR	rv(0x2C)
#define SPFD54124B_CMD_RGBSET	rv(0x2D)

#define SPFD54124B_CMD_MADCTR	rv(0x36)
#define SPFD54124B_CMD_VSCSAD	rv(0x37)
#define SPFD54124B_CMD_COLMOD	rv(0x3A)

#define SPFD54124B_CMD_COLMOD_MCU12bit	rv(3)			// MCU interface 12bit
#define SPFD54124B_CMD_COLMOD_MCU16bit	rv(5)			// MCU interface 16bit
#define SPFD54124B_CMD_COLMOD_MCU18bit	rv(6)			// MCU interface 18bit
#define SPFD54124B_CMD_COLMOD_RGB12bit	rv(30)          // RGB interface 16bit
#define SPFD54124B_CMD_COLMOD_RGB16bit	rv(50)          // RGB interface 16bit
#define SPFD54124B_CMD_COLMOD_RGB18bit	rv(60)			// RGB interface 18bit

#define SPFD54124B_CMD_MADCTR_MY	    rv(1 << 7)		// Row Address Order
#define SPFD54124B_CMD_MADCTR_MX	    rv(1 << 6)		// Column Address Order
#define SPFD54124B_CMD_MADCTR_MV	    rv(1 << 5)		// Row/Column Exchange
#define SPFD54124B_CMD_MADCTR_ML	    rv(1 << 4)		// Vertical Refresh Order
#define SPFD54124B_CMD_MADCTR_RGB	    rv(1 << 3)		// RGB-BGR ORDER

// MY
#define SPFD54124B_CMD_PTLAR	        rv(0x30)
#define SPFD54124B_CMD_SCRLAR	        rv(0x33)
#define SPFD54124B_CMD_RAMHD	        rv(0x2E) // ��������� ������
#define SPFD54124B_CMD_RDDSDR	        rv(0x0F) // ��������� ��� ����� ���������


const U16 init_lcd1616ph [] = {
	SPFD54124B_CMD_SLPOUT,
	SPFD54124B_CMD_COLMOD,
    SPFD54124B_SEND_DATA | SPFD54124B_CMD_COLMOD_MCU16bit,
	SPFD54124B_CMD_DISPON,
	SPFD54124B_CMD_INVOFF,
	SPFD54124B_CMD_NORON,
    //SPFD54124B_CMD_VSCSAD, 0, 1,
};


#if LCD_DMA
U16 LCD_DMA_buf[LCD_DMA_BUF_SIZE*2];
#endif

// ��������� ���������� �������
struct halLCD_lcdStruct_t {
	U8     orientation; // ����������
    COLOR     color;
} halLCD_lcdStruct;


// ��������������� �������� ��� ��������� ������������� LCD

void halLCD_setColor (COLOR color)
{
    halLCD_lcdStruct.color = color;
}

//static __inline 
void halLCD_xspi_rev (U16 data)
{
#if LCD_UART 
    data = __RBIT(data) >> 23;
    while ((USART3->SR & USART_SR_TXE) == 0){}; /* Transmitter register is not empty */
    USART3->DR = data;
#else
    U8 i;
    for (i = 0; i < 9; i++)
    {
        if (data & 0x0100)
        {
            LCD_DAT_H;
        } else {
            LCD_DAT_L;
        }
        data = data << 1;
        LCD_CLK_H;
        __NOP();
	    //if(0 != MMC_MISO) { res = res | 0x01; }  //������� ��� ������
        LCD_CLK_L;
	    __NOP();
    }
#endif
	//return res;
}


void halLCD_sendCmd (U8 data)
{
    LCD_CS_L;
    halLCD_xspi_rev (SPFD54124B_SEND_CMD | data);
    LCD_CS_H;
}


void halLCD_sendData (U8 data)
{
    LCD_CS_L;
    halLCD_xspi_rev (SPFD54124B_SEND_DATA | data);
    LCD_CS_H;
}


void halLCD_cmd1616 (U8 cmd, U16 a, U16 b)
{
    //U8 *pt = (U8 *)a;
	halLCD_sendCmd (cmd);
    halLCD_sendData (a >> 8);  // ������� �������
	halLCD_sendData (a);
    //pt = (U8 *)b;
	halLCD_sendData (b >> 8);    
    halLCD_sendData (b);
}


void halLCD_setWindow (COORD x, COORD y, U16 w, U16 h)
{
    x += LCD_CTRL_X_OFFSET;
    y += LCD_CTRL_Y_OFFSET;
    halLCD_cmd1616 (SPFD54124B_CMD_CASET, x, x + (w - 1)); // column start/end
    halLCD_cmd1616 (SPFD54124B_CMD_RASET, y, y + (h - 1)); // page start/end
	halLCD_sendCmd (SPFD54124B_CMD_RAMWR);
}


//static __inline 
void halLCD_sendPixel (COLOR *color)
{
    U8 *pt = (U8 *)color;
 	halLCD_xspi_rev (SPFD54124B_SEND_DATA | (U16)pt[1]); // ������� �������
 	halLCD_xspi_rev (SPFD54124B_SEND_DATA | (U16)pt[0]); //
//     
  //  halLCD_xspi ((pt[0] <<1) | 0x0001); // ������� �������
//	halLCD_xspi ((pt[1] <<1) | 0x0001); //
}


U32 halLCD_rspi (void) //- not work!
{
    U32 res = 0;
#if LCD_UART 
    
#else
    U8 i;
    for (i = 0; i< (1+ 8*3); i++)
    {
        LCD_CLK_H;//�������
	    res = res << 1;
        __NOP();
	    if (0 != LCD_DAT_IN) { res |= 0x0001; }  //������� ��� ������
        LCD_CLK_L;
	    __NOP();
    }
#endif
	return res;// & 0x00FF;
}


// ��������, ������ ��������!
COLOR halLCD_getPixel (COORD x , COORD y) //- not work!
{
/**
    volatile U16 respond = 0;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    x += 1;
    y += 2;
    
    halLCD_cmd1616(SPFD54124B_CMD_CASET, x, x);
	halLCD_cmd1616(SPFD54124B_CMD_RASET, y, y);	

    //halLCD_sendCmd(SPFD54124B_CMD_RAMHD); 
    U8 i, cod = 0x04;//SPFD54124B_CMD_RAMHD;
    
    // activate lcd by low on CS pin
    LCD_CS_L;
    
    for (i = 0; i< 9 ; i++)
    {
        if (cod & 0x0100)//��������� ��� ������
        {
            LCD_DAT_H; //tr('1');
        } else {
            LCD_DAT_L; //tr('0');
        }
        cod = cod << 1;
        LCD_CLK_H;//�������
	    //res = res << 1;
        //__NOP();
        __NOP();//_delay_ms(1);
	    //if(0 != MMC_MISO) { res = res | 0x01; }  //������� ��� ������
        LCD_CLK_L;
	    __NOP();//_delay_ms(1);
    }
	

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_LCD_DAT;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    respond += halLCD_rspi();
//     respond += halLCD_rspi();
//     respond += halLCD_rspi();
//     respond += halLCD_rspi();
//     respond += halLCD_rspi();
//     respond += halLCD_rspi();
//     respond += halLCD_rspi();
//     respond  = halLCD_rspi() << 8;
//     respond |= halLCD_rspi();


    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_LCD_DAT;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    LCD_CS_H;
    halLCD_sendCmd(SPFD54124B_CMD_NOP);
*/
    return COLOR_BLACK; //respond;
}


void   hal_paint_init (U8 mode)
{
    const U16 *data = &init_lcd1616ph[0];
    U16 size;
    
#ifdef BOARD_TRICORDER_vB
  
#if LCD_UART 
    RCC->APB2ENR |=   (RCC_APB2ENR_IOPDEN);
    
    GPIOD->CRH &= ~(GPIO_CRH_MODE8 | GPIO_CRH_MODE10);   //�������� ������� MODE
    GPIOD->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_CNF10);    //�������� ������� CNF
    GPIOD->CRH |=  (GPIO_CRH_MODE8 | GPIO_CRH_MODE10);   //�����, 50MHz
    GPIOD->CRH |=  (GPIO_CRH_CNF8_1 | GPIO_CRH_CNF10_1);  //�������������� �������, �����������
    
    //TODO  remap!!!!!s
    
    RCC->APB1ENR |= RCC_APB1ENR_USART3EN;
    
    USART3->BRR = 0x0010; /* MAX speed */
    /* 0.5 stop bit, pib CLK ena, polatity and phaze, clock fpr last bit */
    USART3->CR2 =   USART_CR2_STOP_0 | 
                    USART_CR2_CLKEN |
                    USART_CR2_CPOL |
                    USART_CR2_CPHA |
                    USART_CR2_LBCL; 
    /* oversampling 8, 9 bit, transmitter ena, usart ena */   
    USART3->CR1 =   //USART_CR1_OVER8 |
                    USART_CR1_M | //9 bit
                    USART_CR1_TE | //transmitter enable
                    USART_CR1_UE; //
    
#if LCD_DMA
    if ((RCC->AHBENR & RCC_AHBENR_DMA1EN) != RCC_AHBENR_DMA1EN)
        RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    DMA1_Channel2->CPAR  =  (U32) &(USART3->DR); //adress perif
    DMA1_Channel2->CMAR  =  (U32) &LCD_DMA_buf[0];   //adress mem

    DMA1_Channel2->CCR   =  0;          //before clean
    DMA1_Channel2->CCR  = 
        (DMA_CCR_CIRC * 0)              //circ mode off
        | (DMA_CCR_DIR * 1)             //dir - read from mem
        | (DMA_CCR_PSIZE_0 * 1)         //perif size -> 16 bit (9 bit in real)
        | (DMA_CCR_PINC * 0)            //perif -> pointer not inc
        | (DMA_CCR_MSIZE_0 * 1)         //mem size 16 bit
        | (DMA_CCR_MINC * 1);           //mem -> inc pointer

    USART3->CR3 |= USART_CR3_DMAT;      //accses granted for DMA1 to USART3

#endif
    
#else
    GPIO_InitTypeDef GPIO_InitStruct;

    /* GPIO Ports Clock Enable */
    __GPIOD_CLK_ENABLE();
    
    GPIO_InitStruct.Pin = GPIO_PIN_LCD_CLK | GPIO_PIN_LCD_DAT;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
    
    LCD_DAT_L;
    LCD_CLK_L;
#endif

#endif
    LCD_CS_H;
	LCD_RES_L;
	HAL_Delay (10);
	LCD_RES_H;
	HAL_Delay (10);
    
    size = sizeof(init_lcd1616ph) / sizeof(init_lcd1616ph[0]);
    while (size--)
    {
        LCD_CS_L;
        halLCD_xspi_rev (*data++);
        LCD_CS_H;
    }
    HAL_Delay (10);

    hal_paint_setOrientation (mode);
}


void   hal_paint_deinit (U8 mode)
{
    LCD_CS_L;
	LCD_RES_L;
#ifdef BOARD_TRICORDER_vB

#if LCD_UART 
    GPIOD->CRH &= ~(GPIO_CRH_MODE8 | GPIO_CRH_MODE10);
    GPIOD->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_CNF10);
                    
#else
    LCD_DAT_L;
    LCD_CLK_L;
#endif
    
#endif
}


void hal_paint_setPixelColor (COORD x, COORD y, COLOR color) {
    x += LCD_CTRL_X_OFFSET;
    y += LCD_CTRL_Y_OFFSET;
    halLCD_cmd1616 (SPFD54124B_CMD_CASET, x, 1);
	halLCD_cmd1616 (SPFD54124B_CMD_RASET, y, 1);
	halLCD_sendCmd (SPFD54124B_CMD_RAMWR);
    LCD_CS_L;    
	halLCD_sendPixel (&color);
    LCD_CS_H;
}


void hal_paint_setPixel (COORD x, COORD y) {
    x += LCD_CTRL_X_OFFSET;
    y += LCD_CTRL_Y_OFFSET;
    halLCD_cmd1616 (SPFD54124B_CMD_CASET, x, 1);
	halLCD_cmd1616 (SPFD54124B_CMD_RASET, y, 1);
	halLCD_sendCmd (SPFD54124B_CMD_RAMWR);
    LCD_CS_L;    
	halLCD_sendPixel (&halLCD_lcdStruct.color);
    LCD_CS_H;
}


// ������ ������ ����������� ����� �������� � � ����� � ���
void     hal_paint_fillBlock (COORD x, COORD y, U16 w, U16 h, COLOR *buf) {
    U32 n;
    
    n = (w * h);
   	halLCD_setWindow (x, y, w, h);
    LCD_CS_L;
    while(n--) halLCD_sendPixel (buf++);
    LCD_CS_H;
	halLCD_sendCmd (SPFD54124B_CMD_NOP);
}


void     hal_paint_fillBlockColor (COORD x, COORD y, U16 w, U16 h, COLOR color) {
    U32 n;
    
    n = (w * h);
   	halLCD_setWindow (x, y, w, h);
	LCD_CS_L;
#if LCD_DMA
    U32 i;
    for (i = 0; i < (LCD_DMA_BUF_SIZE * 2); i+=2)
    {
        U8 *pt = (U8 *)&color;
        U16 tmp16;
        tmp16 = SPFD54124B_SEND_DATA | (U16)pt[1]; // ������� �������
        LCD_DMA_buf[i + 0] = __RBIT (tmp16) >> 23;
        tmp16 = SPFD54124B_SEND_DATA | (U16)pt[0];
        LCD_DMA_buf[i + 1] = __RBIT (tmp16) >> 23;
    }
    while (n)
    {
        //start
        DMA1_Channel2->CCR  &= ~DMA_CCR_EN;          //disable
        if (n >= LCD_DMA_BUF_SIZE)
        {
            DMA1_Channel2->CNDTR =  LCD_DMA_BUF_SIZE * 2; //reload cntr
            n -= LCD_DMA_BUF_SIZE;
        }
        else
        {
            DMA1_Channel2->CNDTR =  n * 2; //reload cntr
            n -= n;
        }
        DMA1->IFCR          |= DMA_IFCR_CTCIF2;       //clear flag transfer end
        DMA1_Channel2->CCR  |=  DMA_CCR_EN;          //enable
        // wait...
        while ((DMA1->ISR & DMA_ISR_TCIF2) == 0) {};
    }
    while ((USART3->SR & USART_SR_TC) == 0){}; //???? TODO WTF???
    LCD_CS_H;
#else
    while (n--) halLCD_sendPixel (&color);
    LCD_CS_H;
#endif
    
	halLCD_sendCmd (SPFD54124B_CMD_NOP);
}


void     hal_paint_cls (COLOR color) {
    hal_paint_fillBlockColor (0, 0, SCREEN_W, SCREEN_H, color);
}


void     hal_paint_setBackLight (U8 value) {

}


void hal_paint_repaint (void)
{
    // @todo
}


void hal_paint_setOrientation (U8 orient) {
	U8 data = 0;
	halLCD_lcdStruct.orientation = orient;

	switch (orient) {
		case SCREEN_ORIENTATION_90:
			data = SPFD54124B_CMD_MADCTR_MV | SPFD54124B_CMD_MADCTR_MX;
			break;
		case SCREEN_ORIENTATION_180:
			data = SPFD54124B_CMD_MADCTR_MY | SPFD54124B_CMD_MADCTR_MX;
			break;
		case SCREEN_ORIENTATION_270:
			data = SPFD54124B_CMD_MADCTR_MV | SPFD54124B_CMD_MADCTR_MY;
			break;
	}
	halLCD_sendCmd (SPFD54124B_CMD_MADCTR);
	halLCD_sendData (data);
}


U16 hal_paint_getWidth (void) {
	switch (halLCD_lcdStruct.orientation)
    {
		case SCREEN_ORIENTATION_0:
		case SCREEN_ORIENTATION_180:
			return SCREEN_W;
	}
	return SCREEN_H;
}


U16 hal_paint_getHeight (void)
{
	switch (halLCD_lcdStruct.orientation)
    {
		case SCREEN_ORIENTATION_0:
		case SCREEN_ORIENTATION_180:
		return SCREEN_H;
	}
	return SCREEN_W;
}

#endif
