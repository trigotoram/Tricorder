/**
 * @file    _FIFO.h
 * @author  Ht3h5793, CD45
 * @date    13.6.2012  15:50
 * @version V100.30.2
 * @brief   
 * @todo 
 * 
 
 */

#ifndef _FIFO_H
#define	_FIFO_H 20170330

/** ������ ��� "include" */

#include "board.h"


/** ������ ��� "define" */ 
#define FIFO_type						U8
   
/** ����������� ������� ������� */

/** ������ ��� "typedef" */
typedef struct FIFO_t_ { //struct
    S32 in; // Next In Index
    S32 out; // Next Out Index
    FIFO_type *buf; // Buffer
    S32 size;
} FIFO_t;


#ifdef	__cplusplus
extern "C" {
#endif

void FIFO_flush( FIFO_t *s);

BOOL FIFO_init( FIFO_t *s, FIFO_type *buf, U32 FIFO_size);

U32 FIFO_available( FIFO_t *s);

BOOL FIFO_get( FIFO_t *s, FIFO_type *c);

BOOL FIFO_put( FIFO_t *s, FIFO_type c);

BOOL FIFO_gets( FIFO_t *s, FIFO_type *c, U32 data_size);

BOOL FIFO_puts( FIFO_t *s, FIFO_type *c, U32 data_size);


#ifdef	__cplusplus
}
#endif

#endif	/* _FIFO_H */
