/**
 * @file    drvHMC5883L.h
 * @author  ht3v
 * @date    21.12.2015  10.44.42
 * @version V1.0.0
 * @brief 


�������� � ��������

 */
 

#ifndef DRVHMC5883L_H
#define	DRVHMC5883L_H 20151221

#include "board.h"


#ifdef	__cplusplus
extern "C" {
#endif
    

MSG drvHMC5883L_init (U8 mode);
    
MSG drvHMC5883L_get (S16 *X, S16 *Y, S16 *Z);


#ifdef	__cplusplus
}
#endif

#endif	/** DRVHMC5883L_H */
