#include "sysReport.h"
#include "board.h"

#if BOARD_STM32F4DISCOVERY

#ifdef HT3_PAINT
    #include "halPaint.h"
    #include "modPaint.h"
#else
    #include "gfx.h"
#endif
    
#ifdef HT3_PAINT
#else
extern font_t my_font;
#endif
#include "xprintf.h"
extern char _str [4096];

#endif

int printf(const char *_Restrict, ...);

volatile static U16 SYS_REPORT_code;
volatile static U32 SYS_REPORT_position;

//http://catethysis.ru/predefined-macros/
void fERROR_ACTION (U16 code, char *module, U32 position)
{
#if BOARD_STM32F4DISCOVERY
    __DI();
    SYS_REPORT_code = code;
    SYS_REPORT_position = position;
    xsprintf (_str, "E:%X,%u", code, position);
#ifdef HT3_PAINT
    paint_setColor (COLOR_ERROR);
    paint_putStrXY (PAINT_POSX_1, PAINT_POSY_3T, _str);
#else
    gdispDrawString (PAINT_POSX_1, PAINT_POSY_3T, _str, my_font, COLOR_ERROR);
#endif
    printf("\n\nError:%d,%s,%u", code, module, position);
    LED_BLUE_OFF;
    while (1)
    {
        LED_RED_INV;
        for (volatile U32 i = 0; i < 100000; i++) {};
    };
#endif
}


void fWARNING_ACTION (U16 code, char *module, U32 position)
{
#if BOARD_STM32F4DISCOVERY
    SYS_REPORT_code = code;
    SYS_REPORT_position = position;
    xsprintf (_str, "W:%X,%u", code, position);
#ifdef HT3_PAINT
    paint_setColor (COLOR_WARNING);
    paint_putStrXY (PAINT_POSX_1, PAINT_POSY_3T, _str);
#else
    gdispDrawString (PAINT_POSX_1, PAINT_POSY_3T, _str, my_font, COLOR_WARNING);
//    printf("\n\nWarning:%d,%s,%u", code, module, position);
#endif
#endif
    //...
    
}
