/*
 * @file    
 * @author  Ht3h5793
 * @date    10.03.2014
 * @version V9.0.0
 * @brief  
*/

#include <halKey.h>
#include <modKey_local.h>
#include <board.h>

/** �������� ��������� ������ */
#define MODKEY_PIN_STATE_ACTIVE         1
#define MODKEY_PIN_STATE_NOACTIVE       0

// ��� ���� �������� ������ �������������� ���������
U8 halKeyCnt [NUMBER_KEYS];


void halKey_init(void) {
    U8 i;
    __DI();
    // ���������� ������� ������ ���������� �� ���������
    for (i = 0; i < NUMBER_KEYS; i++) {
        halKeyCnt[i] = HALKEY_COUNTER_V_MIN;
    }
    __EI();
}


/**
 * ���������� �������� ���������
 * @param keyNumber - ����� ������.
 * @param pinState - ���������� ��������� ������(������)
 */

U8 halKey_getPinState(U8 pin) {
    U8 PinState = MODKEY_PIN_STATE_NOACTIVE;
    
    switch(pin) { // ���������� ������ � 0!
        case KEY_UP:
            if ((GPIO_KEY_UP) == 0) { // ������� - � GND!
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        case KEY_DOWN:
            if ((GPIO_KEY_DOWN) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        case KEY_LEFT:
            if ((GPIO_KEY_LEFT) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        case KEY_RIGHT:
            if ((GPIO_KEY_RIGHT) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        case KEY_OK:
            if ((GPIO_KEY_OK) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
        
        case KEY_MODE:
            if ((GPIO_KEY_MODE) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
        
        case KEY_SOUND:
            if ((GPIO_KEY_SOUND) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
        
        case KEY_ON:
            if ((GPIO_KEY_ON) == 0) {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        default: break;
    }

    return PinState;
}


void halKey_run (void) {
    U8 i = 0;
    
    for (i = 0; i < NUMBER_KEYS; i++) { // ���������� �������� ���������
        if (MODKEY_PIN_STATE_ACTIVE == halKey_getPinState(i)) {
            halKeyCnt[i]++;
            if (halKeyCnt[i] > HALKEY_COUNTER_V_MAX) {
                halKeyCnt[i] = HALKEY_COUNTER_V_MAX;
            } 
        } else {
            halKeyCnt[i]--;
            if (halKeyCnt[i] < HALKEY_COUNTER_V_MIN) {
                halKeyCnt[i] = HALKEY_COUNTER_V_MIN;
            } 
        }
    }
}


U8 halKey_getCount (U8 num) {
    return halKeyCnt[num];
}
