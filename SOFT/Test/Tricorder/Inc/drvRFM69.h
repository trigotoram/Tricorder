/**
 * @file    drvRFM69.h
 * @author  Ht3h5793
 * @date    09.08.2017  8.19
 * @version V1.0.0
 * @brief 

*/
 

#ifndef DRVRFM69_H
#define	DRVRFM69_H 20170809

/**
 *  ������ ��� "include"
 */
#include "board.h"

/**
 *  ������ ��� "define"
 */
/** @addtogroup RFM69
 * @{
 */
#define RFM69_MAX_PAYLOAD		        64 ///< Maximum bytes payload

/**
 * Valid RFM69 operation modes.
 */
typedef enum
{
  RFM69_MODE_SLEEP = 0,//!< Sleep mode (lowest power consumption)
  RFM69_MODE_STANDBY,  //!< Standby mode
  RFM69_MODE_FS,       //!< Frequency synthesizer enabled
  RFM69_MODE_TX,       //!< TX mode (carrier active)
  RFM69_MODE_RX        //!< RX mode
} RFM69Mode;

/**
 * Valid RFM69 data modes.
 */
typedef enum
{
  RFM69_DATA_MODE_PACKET = 0,                 //!< Packet engine active
  RFM69_DATA_MODE_CONTINUOUS_WITH_SYNC = 2,   //!< Continuous mode with clock recovery
  RFM69_DATA_MODE_CONTINUOUS_WITHOUT_SYNC = 3,//!< Continuous mode without clock recovery
} RFM69DataMode;


/**
 *  ������ ��� "typedef"
 */

#ifdef	__cplusplus
extern "C" {
#endif

/**
 *  ������ ��� ���������� �������
 */
    
/**
 * Enable the +20 dBm high power settings of RFM69Hxx modules.
 *
 * @note Enabling only works with high power devices.
 *
 * @param enable true or false
 */
void RFM69_setHighPowerSettings(BOOL enable);

/**
 * Clear FIFO and flags of RFM69 module.
 */
void RFM69_clearFIFO();

/**
 * Wait until the requested mode is available or timeout.
 */
void RFM69_waitForModeReady();


    
/**
 * Enable/disable the power amplifier(s) of the RFM69 module.
 *
 * PA0 for regular devices is enabled and PA1 is used for high power devices (default).
 *
 * @note Use this function if you want to manually override the PA settings.
 * @note PA0 can only be used with regular devices (not the high power ones!)
 * @note PA1 and PA2 can only be used with high power devices (not the regular ones!)
 *
 * @param forcePA If this is 0, default values are used. Otherwise, PA settings are forced.
 *                0x01 for PA0, 0x02 for PA1, 0x04 for PA2, 0x08 for +20 dBm high power settings.
 */
void RFM69_setPASettings(uint8_t forcePA);


/**
 * Reconfigure the RFM69 module by writing multiple registers at once.
 *
 * @param config Array of register/value tuples
 * @param length Number of elements in config array
 */
void RFM69_setCustomConfig(const uint8_t config[][2], unsigned int length);


/**
 * Read a RFM69 register value.
 *
 * @param reg The register to be read
 * @return The value of the register
 */
uint8_t RFM69_readRegister(uint8_t reg);


/**
 * Write a RFM69 register value.
 *
 * @param reg The register to be written
 * @param value The value of the register to be set
 */
void RFM69_writeRegister(uint8_t reg, uint8_t value);


     /**
 * Switch the mode of the RFM69 module.
 * Using this function you can manually select the RFM69 mode (sleep for example).
 *
 * This function also takes care of the special registers that need to be set when
 * the RFM69 module is a high power device (RFM69Hxx).
 *
 * This function is usually not needed because the library handles mode changes automatically.
 *
 * @param mode RFM69_MODE_SLEEP, RFM69_MODE_STANDBY, RFM69_MODE_FS, RFM69_MODE_TX, RFM69_MODE_RX
 * @return The new mode
 */
RFM69Mode RFM69_setMode(RFM69Mode mode);


#ifdef	__cplusplus
}
#endif

#endif	/** DRVRFM69_H*/
