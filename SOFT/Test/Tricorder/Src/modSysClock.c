#include "modSysClock.h"
#include "board.h"

/**
 * ����������� ��������� �����, ���
 */
#define SYSCLOCK_MAX_TIME               0xFFFFFFFF

/**
 * �������� "�� ���������" ��� ������������ SysClock = 1 ��
 */
#define SYSCLOCK_DEFAULT_PERIOD         TIC_PERIOD   


//====================================================================================================================================================
/**
 * ����������� �������������� ���� � ��������� �������
 * @param pastTime - ����� � �������������
 * @param retFormat - ��������� ������ �������:
 *              SYSCLOCK_GET_TIME_MKS_10         - � �������� �����������
 *              SYSCLOCK_GET_TIME_MKS_100        - � ������ �����������
 *              SYSCLOCK_GET_TIME_MS_1           - � �������������
 *              SYSCLOCK_GET_TIME_MS_10          - � �������� �����������
 *              SYSCLOCK_GET_TIME_MS_100         - � ������ �����������
 *              SYSCLOCK_GET_TIME_S_1            - � ��������
 *              SYSCLOCK_GET_TIME_S_10           - � �������� ������
 * 
 * @return ����� � �������� ���������� �������
 */
SYSTIME modSysClock_formatTime (SYSTIME pastTime, U8 retFormat);
//====================================================================================================================================================


static SYSTIME currentTime;
static SYSTIME modSysClockPeriod = SYSCLOCK_DEFAULT_PERIOD;
//====================================================================================================================================================


/**
 * ������� ��������� ������� ������ 
 * @param Period - ������������� ������ ������� modSysClock_run, ���
 */
void modSysClock_setRunPeriod (SYSTIME period)
{
    modSysClockPeriod = period;
}


/**
 * ���� �������. 
 * ������ ������� ������ ���������� � �������� 1��. 
 */
void  modSysClock_run (void)
{
    currentTime += modSysClockPeriod;
}


/**
 * ������� ���������� ������� �������� �������� �������
 * @return ������� ����� � �������������
 */
SYSTIME modSysClock_getTime (void)
{
    return currentTime;
}


/**
 * ���������� �����, ��������� � ������� startTime � ����������� �������
 * ������������ ������������ �������� currentTime
 * @param startTime - ��������� �������� �������� �������
 * @param retFormat - ������ ������������� �������:
 *              SYSCLOCK_GET_TIME_MKS_10         - � �������� �����������
 *              SYSCLOCK_GET_TIME_MKS_100        - � ������ �����������
 *              SYSCLOCK_GET_TIME_MS_1           - � �������������
 *              SYSCLOCK_GET_TIME_MS_10          - � �������� �����������
 *              SYSCLOCK_GET_TIME_MS_100         - � ������ �����������
 *              SYSCLOCK_GET_TIME_S_1            - � ��������
 *              SYSCLOCK_GET_TIME_S_10           - � �������� ������
 * @return �����, ��������� �� startTime, � ��������� ��������
 */
SYSTIME modSysClock_getPastTime (SYSTIME startTime, U8 retFormat)
{
    SYSTIME pastTime = 0;

    if (startTime <= currentTime)
    {
        pastTime = currentTime - startTime;
    } else {
        pastTime = (SYSTIME)SYSCLOCK_MAX_TIME - startTime + currentTime + 1;
    }
    
    pastTime = modSysClock_formatTime (pastTime, retFormat);
    
    return pastTime;
}


/**
 * ����������� �������������� ���� � ��������� �������
 * @param pastTime - ����� � �������������
 * @param retFormat - ��������� ������ �������:
 *              SYSCLOCK_GET_TIME_MKS_10         - � �������� �����������
 *              SYSCLOCK_GET_TIME_MKS_100        - � ������ �����������
 *              SYSCLOCK_GET_TIME_MS_1           - � �������������
 *              SYSCLOCK_GET_TIME_MS_10          - � �������� �����������
 *              SYSCLOCK_GET_TIME_MS_100         - � ������ �����������
 *              SYSCLOCK_GET_TIME_S_1            - � ��������
 *              SYSCLOCK_GET_TIME_S_10           - � �������� ������
 * 
 * @return ����� � �������� ���������� �������
 */
SYSTIME modSysClock_formatTime (SYSTIME pastTime, U8 retFormat)
{
    U32 retVal = 0;
    U32 divider = 1;
    
    switch (retFormat)
    {
        case SYSCLOCK_GET_TIME_MKS_10:
            divider = (TIC_PERIOD / 100);
            break;
        
        case SYSCLOCK_GET_TIME_MKS_100:
            divider = (TIC_PERIOD / 10);
            break;
        
        case SYSCLOCK_GET_TIME_MS_1:
            divider = (TIC_PERIOD);
            break;
        
        case SYSCLOCK_GET_TIME_MS_10:
            divider = (TIC_PERIOD * 10);
            break;
        
        case SYSCLOCK_GET_TIME_MS_100:
            divider = (TIC_PERIOD * 100);
            break;
        
        case SYSCLOCK_GET_TIME_S_1:
            divider = (TIC_PERIOD * 1000);
            break;
        
        case SYSCLOCK_GET_TIME_S_10:
            divider = (TIC_PERIOD * 10000);
            break;
        
        default: break;
    }
            
    retVal = pastTime / divider;
    
    return retVal;
}


MSG modSysClock_timeout (SYSTIME *pdelay, SYSTIME delay, U8 retFormat)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    
    if (modSysClock_getPastTime (*pdelay, retFormat) >= delay)
    {  
        *pdelay = modSysClock_getTime();
        respond = FUNCTION_RETURN_OK;
    }
    
    return respond;
}


void modSysClock_wait (SYSTIME delay, U8 retFormat)
{
    SYSTIME delaytmp = modSysClock_getTime();
    while (1)
    {
        if (modSysClock_getPastTime (delaytmp, retFormat) >= delay)
        {
            break;
        }
    }
}
