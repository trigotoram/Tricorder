/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define GPIO_Input_KEY_RIGHT_Pin GPIO_PIN_2
#define GPIO_Input_KEY_RIGHT_GPIO_Port GPIOE
#define GPIO_Input_KEY_DOWN_Pin GPIO_PIN_3
#define GPIO_Input_KEY_DOWN_GPIO_Port GPIOE
#define GPIO_Input_KEY_MODE_Pin GPIO_PIN_4
#define GPIO_Input_KEY_MODE_GPIO_Port GPIOE
#define GPIO_Input_KEY_SOUND_Pin GPIO_PIN_5
#define GPIO_Input_KEY_SOUND_GPIO_Port GPIOE
#define GPIO_Input_KEY_OK_Pin GPIO_PIN_6
#define GPIO_Input_KEY_OK_GPIO_Port GPIOE
#define GPIO_Input_KEY_ON_Pin GPIO_PIN_13
#define GPIO_Input_KEY_ON_GPIO_Port GPIOC
#define GPIO_Out_S0_Pin GPIO_PIN_5
#define GPIO_Out_S0_GPIO_Port GPIOC
#define GPIO_Out_S1_Pin GPIO_PIN_0
#define GPIO_Out_S1_GPIO_Port GPIOB
#define GPIO_Out_BT_RES_Pin GPIO_PIN_1
#define GPIO_Out_BT_RES_GPIO_Port GPIOB
#define GPIO_Output_BT_MODE_Pin GPIO_PIN_2
#define GPIO_Output_BT_MODE_GPIO_Port GPIOB
#define GPIO_Out_GPS_ON_Pin GPIO_PIN_7
#define GPIO_Out_GPS_ON_GPIO_Port GPIOE
#define GPIO_Out_WIFI_ON_Pin GPIO_PIN_8
#define GPIO_Out_WIFI_ON_GPIO_Port GPIOE
#define GPIO_Out_PWRVDD_ON_Pin GPIO_PIN_9
#define GPIO_Out_PWRVDD_ON_GPIO_Port GPIOE
#define GPIO_Output_RFM69_CS_Pin GPIO_PIN_10
#define GPIO_Output_RFM69_CS_GPIO_Port GPIOE
#define GPIO_Out_RFM69_RES_Pin GPIO_PIN_11
#define GPIO_Out_RFM69_RES_GPIO_Port GPIOE
#define GPIO_Input_RFM69_IRQ_Pin GPIO_PIN_12
#define GPIO_Input_RFM69_IRQ_GPIO_Port GPIOE
#define GPIO_Output_NRF24_CS_Pin GPIO_PIN_13
#define GPIO_Output_NRF24_CS_GPIO_Port GPIOE
#define GPIO_Output_NRF24_CE_Pin GPIO_PIN_14
#define GPIO_Output_NRF24_CE_GPIO_Port GPIOE
#define GPIO_Input_NRF24_IRQ_Pin GPIO_PIN_15
#define GPIO_Input_NRF24_IRQ_GPIO_Port GPIOE
#define GPIO_Output_SPIFLASH_CS_Pin GPIO_PIN_12
#define GPIO_Output_SPIFLASH_CS_GPIO_Port GPIOB
#define GPIO_Out_GSM_ON_Pin GPIO_PIN_13
#define GPIO_Out_GSM_ON_GPIO_Port GPIOD
#define GPIO_Out_LCD_ON_Pin GPIO_PIN_14
#define GPIO_Out_LCD_ON_GPIO_Port GPIOD
#define GPIO_Out_VIBRO_Pin GPIO_PIN_8
#define GPIO_Out_VIBRO_GPIO_Port GPIOA
#define GPIO_Out_USB_RENUM_Pin GPIO_PIN_0
#define GPIO_Out_USB_RENUM_GPIO_Port GPIOD
#define GPIO_Out_LCD_CS_Pin GPIO_PIN_3
#define GPIO_Out_LCD_CS_GPIO_Port GPIOD
#define GPIO_Out_LCD_RES_Pin GPIO_PIN_4
#define GPIO_Out_LCD_RES_GPIO_Port GPIOD
#define GPIO_Out_LCD_RS_Pin GPIO_PIN_5
#define GPIO_Out_LCD_RS_GPIO_Port GPIOD
#define GPIO_Out_SENSOR_ON_Pin GPIO_PIN_7
#define GPIO_Out_SENSOR_ON_GPIO_Port GPIOD
#define GPIO_Input_I2C_IRQ_Pin GPIO_PIN_4
#define GPIO_Input_I2C_IRQ_GPIO_Port GPIOB
#define GPIO_Input_KEY_LEFT_Pin GPIO_PIN_0
#define GPIO_Input_KEY_LEFT_GPIO_Port GPIOE
#define GPIO_Input_KEY_UP_Pin GPIO_PIN_1
#define GPIO_Input_KEY_UP_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

      


#define GPIO_Out_S0_Pin GPIO_PIN_5
#define GPIO_Out_S0_GPIO_Port GPIOC
#define GPIO_Out_S1_Pin GPIO_PIN_0
#define GPIO_Out_S1_GPIO_Port GPIOB
#define GPIO_Out_BT_RES_Pin GPIO_PIN_1
#define GPIO_Out_BT_RES_GPIO_Port GPIOB
#define GPIO_Out_BT_MODE_Pin GPIO_PIN_2
#define GPIO_Out_BT_MODE_GPIO_Port GPIOB
#define GPIO_Out_GPS_ON_Pin GPIO_PIN_7
#define GPIO_Out_GPS_ON_GPIO_Port GPIOE
#define GPIO_Out_WIFI_RES_Pin GPIO_PIN_8
#define GPIO_Out_WIFI_RES_GPIO_Port GPIOE
#define GPIO_Out_PWRVDD_ON_Pin GPIO_PIN_9
#define GPIO_Out_PWRVDD_ON_GPIO_Port GPIOE
#define GPIO_Out_RFM69_CS_Pin GPIO_PIN_10
#define GPIO_Out_RFM69_CS_GPIO_Port GPIOE
#define GPIO_Out_RFM69_RES_Pin GPIO_PIN_11
#define GPIO_Out_RFM69_RES_GPIO_Port GPIOE
#define GPIO_Input_RFM69_IRQ_Pin GPIO_PIN_12
#define GPIO_Input_RFM69_IRQ_GPIO_Port GPIOE
#define GPIO_Out_NRF24_CS_Pin GPIO_PIN_13
#define GPIO_Out_NRF24_CS_GPIO_Port GPIOE
#define GPIO_Out_NRF24_CE_Pin GPIO_PIN_14
#define GPIO_Out_NRF24_CE_GPIO_Port GPIOE
#define GPIO_Input_NRF24_IRQ_Pin GPIO_PIN_15
#define GPIO_Input_NRF24_IRQ_GPIO_Port GPIOE
#define GPIO_Out_SPIMEM_CS_Pin GPIO_PIN_12
#define GPIO_Out_SPIMEM_CS_GPIO_Port GPIOB
#define GPIO_Out_GSM_ON_Pin GPIO_PIN_13
#define GPIO_Out_GSM_ON_GPIO_Port GPIOD
#define GPIO_Out_LCD_ON_Pin GPIO_PIN_14
#define GPIO_Out_LCD_ON_GPIO_Port GPIOD
#define GPIO_Out_VIBRO_Pin GPIO_PIN_8
#define GPIO_Out_VIBRO_GPIO_Port GPIOA
#define GPIO_Out_USB_RENUM_Pin GPIO_PIN_0
#define GPIO_Out_USB_RENUM_GPIO_Port GPIOD
#define GPIO_Out_LCD_CS_Pin GPIO_PIN_3
#define GPIO_Out_LCD_CS_GPIO_Port GPIOD
#define GPIO_Out_LCD_RES_Pin GPIO_PIN_4
#define GPIO_Out_LCD_RES_GPIO_Port GPIOD
#define GPIO_Out_LCD_RS_Pin GPIO_PIN_5
#define GPIO_Out_LCD_RS_GPIO_Port GPIOD
#define GPIO_Out_SENSOR_ON_Pin GPIO_PIN_7
#define GPIO_Out_SENSOR_ON_GPIO_Port GPIOD
#define GPIO_Input_I2C_IRQ_Pin GPIO_PIN_4
#define GPIO_Input_I2C_IRQ_GPIO_Port GPIOB



/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
