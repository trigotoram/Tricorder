#include "game_worm.h"
#include "modPaint.h"
#include "modKey.h"
#include "modSysClock.h"
#include "modRandom.h"

#include "conv.h"
#include "_debug.h" // ��� ��������


#define paint_cell(x,y)                 paint_rect(x - WORM_CELL_SIZE/2 , y - WORM_CELL_SIZE/2, WORM_CELL_SIZE, WORM_CELL_SIZE);
#define WORM_UP_BORDER_COORD            (WORM_CELL_SIZE * 4)

extern void drvBUZZER_peep (U8 numbers);

typedef enum {
    DIR_RIGHT = 1,
    DIR_LEFT,
    DIR_DOWN,
    DIR_UP
} GAME_DIRECTION;

typedef struct {
    S16 x;
    S16 y;
} POSITION;

typedef struct {
    U8  dir;
    U32  size;
    SYSTIME tic_timer;
    SYSTIME tic_timeout;
    POSITION head;
    POSITION body[256];
    POSITION food;
    U32 score;
    U16 sound;
    SYSTIME sound_timer;
} worm_t; //
worm_t game;


void  worm_food_new (void)
{
regenerate:
    game.food.x = _rand32 () % (paint_getWidth () / WORM_CELL_SIZE) * WORM_CELL_SIZE;
    game.food.y = _rand32 () % (paint_getHeight () / WORM_CELL_SIZE) * WORM_CELL_SIZE;
    if ((game.food.x < (WORM_CELL_SIZE*4)) ||
        (game.food.y < (WORM_CELL_SIZE*9)) ||
        (game.food.x > (paint_getWidth () - WORM_CELL_SIZE*3)) ||
        (game.food.y > (paint_getHeight () - WORM_CELL_SIZE*3)))
        goto regenerate;
}


MSG  worm_collision (void)
{
    U32 i;
    MSG respond = FALSE;
    
//     for (i = 0; i < game.size; i++) {
//         if ((game.y == game.body[i].x) && (game.y == game.body[i].y)) {
//             respond = TRUE;
//             break;
//         }
//     }
    return respond;
}


void game_worm_init (void)
{
    U32 i, j;
    
    game.head.x = WORM_CELL_SIZE * 6; //paint_getWidth() / 2;
    game.head.y = WORM_CELL_SIZE * 5; //paint_getHeight() / 2;
    game.size = 4;
    for (i = 0; i < 256; i++) {
        game.body[i].x = 1111;
        game.body[i].y = 1111;
    }
    for (i = 0; i < game.size; i++) {
        game.body[i].x = game.head.x;
        game.body[i].y = game.head.y;
    }
    game.tic_timer = 0;
    game.tic_timeout = 200;
    game.dir = DIR_RIGHT;
    game.score = 0;
    // 
    worm_food_new ();
    game.sound = 0;
    //
    paint_setBackgroundColor (COLOR_BLACK);
    paint_clearScreen ();
    
    //draw borders
    paint_setColor (COLOR_SILVER);
    j = WORM_UP_BORDER_COORD;
    for (i = 0; i < (paint_getWidth () - WORM_CELL_SIZE); i += WORM_CELL_SIZE)
    {
        paint_cell (i, j);
    }
    for (; j < (paint_getHeight () - WORM_CELL_SIZE); j += WORM_CELL_SIZE)
    {
        paint_cell (i, j);
    }
    for (; i > WORM_CELL_SIZE; i -= WORM_CELL_SIZE)
    {
        paint_cell (i, j);
    }
    for (; j > WORM_UP_BORDER_COORD; j -= WORM_CELL_SIZE)
    {
        paint_cell (i, j);
    }
    
    // repaint body
    paint_setColor (COLOR_BLUE);
    for (i = 0; i < game.size; i++)
    {
        paint_cell (game.body[i].x, game.body[i].y);
    }
    paint_setColor (COLOR_GREEN);
    paint_cell (game.head.x, game.head.y); // repaint head
    paint_setColor (COLOR_RED);
    paint_cell (game.food.x, game.food.y); // repaint food
    
    //paint_repaint ();
}


void game_worm_run (void)
{
    U32 i;
    char strr[32];
    
    modKey_run();
    
    if (MODKEY_STATE_PRESSED == modKey_getState (KEY_RIGHT))
    {
        game.dir = DIR_RIGHT;
    }
    if (MODKEY_STATE_PRESSED == modKey_getState (KEY_LEFT))
    {
        game.dir = DIR_LEFT;
    }
    if (MODKEY_STATE_PRESSED == modKey_getState (KEY_DOWN))
    {
        game.dir = DIR_DOWN;
    }
    if (MODKEY_STATE_PRESSED == modKey_getState (KEY_UP))
    {
        game.dir = DIR_UP;
    }
    if (MODKEY_STATE_PRESSED == modKey_getState (KEY_OK))
    {
        if (DIR_RIGHT == game.dir)
            game.dir = DIR_DOWN;
        else if (DIR_DOWN == game.dir)
            game.dir = DIR_LEFT;
        else if (DIR_LEFT == game.dir)
            game.dir = DIR_UP;
        else if (DIR_UP == game.dir)
            game.dir = DIR_RIGHT;
    }

    if (modSysClock_getPastTime (game.tic_timer, SYSCLOCK_GET_TIME_MS_1) > game.tic_timeout)
    {   
        game.tic_timer = modSysClock_getTime();
        
        //clean all painting
        //paint_setBackgroundColor (COLOR_BLACK);
        //paint_clearScreen ();
        paint_setColor (COLOR_BLACK);
        /*
        for (i = 0; i < game.size; i++)
        {
            paint_cell (game.body[i].x, game.body[i].y);
        }*/
        paint_cell (game.body[game.size -1].x, game.body[game.size - 1].y);
        paint_cell (game.head.x, game.head.y); // head
        //paint_cell (game.food.x, game.food.y); // food
        
        
        // move body
        for (i = (game.size - 1); i >= 1; i--)
        {
            game.body[i].x = game.body[i -1].x;
            game.body[i].y = game.body[i -1].y;
        }
        game.body[0].x = game.head.x;
        game.body[0].y = game.head.y;
        //move head
        switch (game.dir)
        {
            case DIR_RIGHT:
                game.head.x = game.head.x + WORM_CELL_SIZE;
                break;
            
            case DIR_LEFT:
                game.head.x = game.head.x - WORM_CELL_SIZE;
                break;
            
            case DIR_DOWN:
                game.head.y = game.head.y + WORM_CELL_SIZE;
                break;
            
            case DIR_UP:
                game.head.y = game.head.y - WORM_CELL_SIZE;
                break;
        }
        //check collision
        if (game.head.x >= (paint_getWidth () - WORM_CELL_SIZE) || 
            game.head.y >= (paint_getHeight () - WORM_CELL_SIZE) || 
            game.head.x < 0 || 
            game.head.y < WORM_UP_BORDER_COORD ||
            worm_collision ())
        {
            HAL_Delay (1000);
            game_worm_init ();
        }
        
        //eat food?
        if ((game.head.x == game.food.x) && 
            (game.head.y == game.food.y))
        {
            paint_setColor (COLOR_BLACK);
            paint_cell (game.food.x, game.food.y);
            worm_food_new ();
            game.score++;
            paint_setColor (COLOR_WHITE);
            xsprintf (strr, "Score:%05u", game.score);
            paint_putStrXY (0, 0, strr);
            game.size++;
            drvBUZZER_peep(1);
        }

        // repaint body
        paint_setColor (COLOR_BLUE);
        /*
        for (i = 0; i < game.size; i++)
        {
            paint_cell (game.body[i].x, game.body[i].y);
        }*/
        paint_cell (game.body[0].x, game.body[0].y);//paint_cell (game.body[game.size -1].x, game.body[game.size - 1].y);
        paint_setColor (COLOR_GREEN);
        paint_cell (game.head.x, game.head.y); // repaint head
        //paint_setColor (COLOR_RED);
        //paint_cell (game.food.x, game.food.y); // repaint food
        
        
        
        //paint_repaint ();
    }
    /*
    if (modSysClock_getPastTime (game.sound_timer, SYSCLOCK_GET_TIME_MS_1) > 10)
    {   
        game.sound_timer = modSysClock_getTime();
        // sound
        if (0 != game.sound)
        {
            game.sound--;
            drvBUZZER_peep(1);
        }
    }
    */
}

