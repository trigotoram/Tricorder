#include "drvSHT21.h"
#include "board.h"
#include "halI2C.h"
#include "_debug.h"

#if (I2C_SHT21)

#define I2C_SHT21_ADRESS                (0x80)


MSG    drvSHT21_setMode (U8 mode)
{
    MSG respond = FUNCTION_RETURN_ERROR;

    
    return respond;
}


MSG    drvSHT21_getTemperature (S8 *tempI, U8 *tempF)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[3];
	//int16_t tmp16;
    U32 tmp32;
    float tmpF;
    
    //*tempI = 20;
    //*tempF = 0;
    I2C_buf[0] = 0xF3; //CMD - start measure temperature
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_SHT21_ADRESS, &I2C_buf[0], 1, 0))
    {
        _delay_ms (80);
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_SHT21_ADRESS, &I2C_buf[0], 3, 0)) //+crc
        {
            tmp32 = (((uint16_t)I2C_buf[0] << 8) | I2C_buf[1]); //combine world
            //tmp32 = tmp32 >> 2;
            
            tmpF = tmp32;
            tmpF = tmpF / 65536.0;
            tmpF = tmpF * 175.72;
            tmpF = -46.85 + tmpF;
            
            /*
            tmp32 = tmp32 << 16;
            tmp32 *= 11515985; // 11515985.92 = (175.72 * 65536)
            tmp32 -= 3070361; // -3070361.6 = (-46.85 * 65536)
            tmp32 = tmp32 >> 16;
            */
            *tempI = (U8)tmpF;
            
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


MSG    drvSHT21_getHumidity (U8 *humidity)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[3];
    U32 tmp32;
    float tmpF;
    
    I2C_buf[0] = 0xF5; //CMD - start measure humidity
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_SHT21_ADRESS, &I2C_buf[0], 1, 0))
    {
        _delay_ms (30);
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_SHT21_ADRESS, &I2C_buf[0], 3, 0)) //+crc
        {
            tmp32 = (((uint16_t)I2C_buf[0] << 8) | I2C_buf[1]); //combine world
            //tmp32 = tmp32 >> 2;
            
            tmpF = tmp32;
            tmpF = tmpF / 65536.0;
            tmpF = tmpF * 125;
            tmpF = -6 + tmpF;
            
            /*
            tmp32 = tmp32 << 16;
            tmp32 *= 11515985; // 11515985.92 = (175.72 * 65536)
            tmp32 -= 3070361; // -3070361.6 = (-46.85 * 65536)
            tmp32 = tmp32 >> 16;
            */
            *humidity = (U8)tmpF;
            
            
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}

#endif
