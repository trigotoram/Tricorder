/*
 * @file    
 * @author  Ht3h5793
 * @date    10.03.2014
 * @version V9.0.0
 * @brief  
*/

#include "halKey.h"
#include "modKey_local.h"
#include "board.h"

/** �������� ��������� ������ */
#define MODKEY_PIN_STATE_ACTIVE         1
#define MODKEY_PIN_STATE_NOACTIVE       0

// ��� ���� �������� ������ �������������� ���������
uint8_t halKeyCnt [NUMBER_KEYS];


void halKey_init (void)
{
    uint8_t i;
    
#ifdef BOARD_BIMBA
    __HAL_RCC_GPIOB_CLK_ENABLE ();
    __HAL_RCC_GPIOC_CLK_ENABLE ();
  
    // BUTTON_1
    //���������������� GPIOB.7
    GPIOB->CRL &= ~GPIO_CRL_MODE7;   //�������� ������� MODE
    GPIOB->CRL &= ~GPIO_CRL_CNF7;    //�������� ������� CNF
    GPIOB->CRL |=  GPIO_CRL_CNF7_1;  //���������� ����, �������� � "�����"
    GPIOB->BSRR =  GPIO_BSRR_BR7;    //�������� ������������� ��������
    
    // BUTTON_3
    //���������������� GPIOB.12
    GPIOB->CRH &= ~GPIO_CRH_MODE12;  //�������� ������� MODE
    GPIOB->CRH &= ~GPIO_CRH_CNF12;   //�������� ������� CNF
    GPIOB->CRH |=  GPIO_CRH_CNF12_1; //���������� ����, �������� � "�����"
    GPIOB->BSRR =  GPIO_BSRR_BR12;   //�������� ������������� ��������

    // BUTTON_3
    //���������������� GPIOB.9
    GPIOB->CRH &= ~GPIO_CRH_MODE9;   //�������� ������� MODE
    GPIOB->CRH &= ~GPIO_CRH_CNF9;    //�������� ������� CNF
    GPIOB->CRH |=  GPIO_CRH_CNF9_1;  //���������� ����, �������� � "�����"
    GPIOB->BSRR =  GPIO_BSRR_BR9;    //�������� ������������� ��������
    
#endif
    
    // ���������� ������� ������ ���������� �� ���������
    for (i = 0; i < NUMBER_KEYS; i++)
    {
        halKeyCnt [i] = HALKEY_COUNTER_V_MIN;
    }
}


/**
 * ���������� �������� ���������
 * @param keyNumber - ����� ������.
 * @param pinState - ���������� ��������� ������(������)
 */
uint8_t halKey_getPinState (uint8_t pin)
{
    uint8_t PinState = MODKEY_PIN_STATE_NOACTIVE;
    
    switch (pin) // ���������� ������ � 0!
    {
#ifdef BOARD_BIMBA
        case BUTTON_1: //BUTTON_1
            if (0 != (GPIOB->IDR &  GPIO_PIN_7)) // ������� - � VCC!
            {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        case BUTTON_2: //BUTTON_2
            if (0 != (GPIOB->IDR &  GPIO_PIN_12)) // ������� - � VCC!
            {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;
            
        case BUTTON_3: //BUTTON_3
            if (0 != (GPIOB->IDR &  GPIO_PIN_9)) // ������� - � VCC!
            {
                PinState =  MODKEY_PIN_STATE_ACTIVE;
            }
            break;

#endif

        default: break;
    }

    return PinState;
}


// https://avrlab.com/node/85
static const char key_code [4][3] = { //���� ������
    {'1','2','3'},
    {'4','5','6'},
    {'7','8','9'},
    {'*','0','#'}
};

void halKey_inc (uint8_t i)
{
    halKeyCnt [i]++;
    if (halKeyCnt [i] > HALKEY_COUNTER_V_MAX)
    {
        halKeyCnt [i] = HALKEY_COUNTER_V_MAX;
    }
}


void halKey_dec (uint8_t i)
{
    halKeyCnt[i]--;
    if (halKeyCnt [i] < HALKEY_COUNTER_V_MIN)
    {
        halKeyCnt [i] = HALKEY_COUNTER_V_MIN;
    }
}


void halKey_run (void) //TODO ���������� �� ������ ���� ������
{
    uint8_t i = 0;
    uint8_t  x, y;
    
    //���������������� GPIOC.4,5,6 �� ����
    GPIOC->CRL &= ~(GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6);   //�������� ������� MODE
    GPIOC->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_CNF5 | GPIO_CRL_CNF6);    //�������� ������� CNF
    GPIOC->CRL |=  (GPIO_CRL_CNF4_1 | GPIO_CRL_CNF5_1 | GPIO_CRL_CNF6_1);  //���������� ����, �������� � �����
    GPIOC->BSRR =  (GPIO_BSRR_BS4 | GPIO_BSRR_BS5 | GPIO_BSRR_BS6);    //�������� ������������� ��������

    //���������������� GPIOC.7 �� �����
    GPIOC->CRL &= ~GPIO_CRL_MODE7;   //�������� ������� MODE
    GPIOC->CRL &= ~GPIO_CRL_CNF7;    //�������� ������� CNF
    GPIOC->CRL |=  GPIO_CRL_MODE7;   //�����, 50MHz
    GPIOC->CRL |=  GPIO_CRL_CNF7_0;  //������ ����������, �������� ����
    GPIOC->BSRR =  GPIO_BSRR_BR7;  //GPIOC.7=0
    //���������������� GPIOC.8,9  �� �����
    GPIOC->CRH &= ~(GPIO_CRH_MODE8 | GPIO_CRH_MODE9);   //�������� ������� MODE
    GPIOC->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_CNF9);    //�������� ������� CNF
    GPIOC->CRH |=  (GPIO_CRH_MODE8 | GPIO_CRH_MODE9);   //�����, 50MHz
    GPIOC->CRH |=  (GPIO_CRH_CNF8_0 | GPIO_CRH_CNF9_0);  //������ ����������, �������� ����
    GPIOC->BSRR =  GPIO_BSRR_BR8 | GPIO_BSRR_BR9;  //GPIOC.8=0
    //���������������� GPIOB.6
    GPIOB->CRL &= ~GPIO_CRL_MODE6;   //�������� ������� MODE
    GPIOB->CRL &= ~GPIO_CRL_CNF6;    //�������� ������� CNF
    GPIOB->CRL |=  GPIO_CRL_MODE6;   //�����, 50MHz
    GPIOB->CRL |=  GPIO_CRL_CNF6_0;  //������ ����������, �������� ����
    GPIOB->BSRR =  GPIO_BSRR_BR6;  //GPIOB.6=0

    //for (i = 0; i < 10; i++) {}; //�������� ��� ���������� ������ ���������� ���������, ����� �� �� ������!
    for (i = BUTTON_1; i < BUTTON_1 + 3; i++) // ���������� �������� ��������� ������
    {
        if (MODKEY_PIN_STATE_ACTIVE == halKey_getPinState (i))
        {
            halKey_inc (i);
        }
        else
        {
            halKey_dec (i); 
        }
    }
    
    x = 3;
    if (0 == (GPIOC->IDR & GPIO_PIN_4)) x = 0; //���� ������ ������� � 0� �������
    if (0 == (GPIOC->IDR & GPIO_PIN_5)) x = 1; //���� ������ ������� � 1� �������
    if (0 == (GPIOC->IDR & GPIO_PIN_6)) x = 2; //���� ������ ������� � 3� �������
    
    //short circout - not allowed!
    //���������������� GPIOC.4,5,6 �� �����
    GPIOC->CRL &= ~(GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6);   //�������� ������� MODE
    GPIOC->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_CNF5 | GPIO_CRL_CNF6);    //�������� ������� CNF
    GPIOC->CRL |=  (GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6);   //�����, 50MHz
    GPIOC->CRL |=  (GPIO_CRL_CNF4_0 | GPIO_CRL_CNF5_0 | GPIO_CRL_CNF6_0);  //������ ����������, �������� ����
    GPIOC->BSRR =  (GPIO_BSRR_BR4 | GPIO_BSRR_BR5 | GPIO_BSRR_BR6);  //GPIOC.4=0

    //���������������� GPIOC.7 �� ����
    GPIOC->CRL &= ~GPIO_CRL_MODE7;   //�������� ������� MODE
    GPIOC->CRL &= ~GPIO_CRL_CNF7;    //�������� ������� CNF
    GPIOC->CRL |=  GPIO_CRL_CNF7_1;  //���������� ����, �������� � �����
    GPIOC->BSRR =  GPIO_BSRR_BS7;    //�������� ������������� ��������
    //���������������� GPIOC.8,9 �� ����
    GPIOC->CRH &= ~(GPIO_CRH_MODE8 | GPIO_CRH_MODE9);   //�������� ������� MODE
    GPIOC->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_CNF9);    //�������� ������� CNF
    GPIOC->CRH |=  (GPIO_CRH_CNF8_1 | GPIO_CRH_CNF9_1);  //���������� ����, �������� � �����
    GPIOC->BSRR =  (GPIO_BSRR_BS8 | GPIO_BSRR_BS9);    //�������� ������������� ��������
    //���������������� GPIOB.6 �� ����
    GPIOB->CRL &= ~GPIO_CRL_MODE6;   //�������� ������� MODE
    GPIOB->CRL &= ~GPIO_CRL_CNF6;    //�������� ������� CNF
    GPIOB->CRL |=  GPIO_CRL_CNF6_1;  //���������� ����, �������� � �����
    GPIOB->BSRR =  GPIO_BSRR_BS6;    //�������� ������������� ��������

    for (i = 0; i < 10; i++) {}; //�������� ��� ���������� ������ ���������� ���������, ����� �� �� ������!
    
    //y = 4;
    if (0 == (GPIOC->IDR & GPIO_PIN_7)) ////���� ������ ������� � 0� ������, j=0
    {
        if (0 == x) halKey_inc (KEY_1);
        if (1 == x) halKey_inc (KEY_2);
        if (2 == x) halKey_inc (KEY_3);
    }
    else
    {
        halKey_dec (KEY_1);
        halKey_dec (KEY_2);
        halKey_dec (KEY_3);
    }
    if (0 == (GPIOC->IDR & GPIO_PIN_8))
    {
        if (0 == x) halKey_inc (KEY_4);
        if (1 == x) halKey_inc (KEY_5);
        if (2 == x) halKey_inc (KEY_6);  
    }
    else
    {
        halKey_dec (KEY_4);
        halKey_dec (KEY_5);
        halKey_dec (KEY_6);
    }
    if (0 == (GPIOC->IDR & GPIO_PIN_9))
    {
        if (0 == x) halKey_inc (KEY_7);
        if (1 == x) halKey_inc (KEY_8);
        if (2 == x) halKey_inc (KEY_9);
    }
    else
    {
        halKey_dec (KEY_7);
        halKey_dec (KEY_8);
        halKey_dec (KEY_9);
    }
    if (0 == (GPIOB->IDR & GPIO_PIN_6))
    {
        if (0 == x) halKey_inc (KEY_STAR);
        if (1 == x) halKey_inc (KEY_0);
        if (2 == x) halKey_inc (KEY_SHARP);
    }
    else
    {
        halKey_dec (KEY_STAR);
        halKey_dec (KEY_0);
        halKey_dec (KEY_SHARP);
    }

//    if ((x != 3) && (y != 4)) //���� ���� ������ �������
//    {
//        //xsprintf (_str, "%c %2u", key_code [y][x], key_code [y][x]);
//        _str [0] = '0' + y;
//        _str [1] = '0' + x;
//        _str [2] = key_code [y][x];
//
////        _str [1] = '0' +halKeyCnt [0];
////        _str [2] = '0' +halKeyCnt [1];
////        _str [3] = '0' +halKeyCnt [2];
//    } 
//    else
//    {
//        _str [0] = '0';
//        _str [1] = '0';
//        _str [2] = '0';
//    }
//    
}


uint8_t halKey_getCount (uint8_t num)
{
    return halKeyCnt [num];
}
