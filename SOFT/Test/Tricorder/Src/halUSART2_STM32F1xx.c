#include "halUSART.h"
#include "board.h"


#ifdef HAL_USART2

#define TBUF2_SIZE                      (USART2_TBUF_SIZE / 2) /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/
#define RBUF2_SIZE                      (USART2_RBUF_SIZE / 2) /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/

#if TBUF2_SIZE < 2
    #error "TBUF2_SIZE is too small.  It must be larger than 1."
#elif ((TBUF2_SIZE & (TBUF2_SIZE - 1)) != 0)
    #error "TBUF2_SIZE must be a power of 2."
#endif

#if RBUF2_SIZE < 2
    #error "RBUF2_SIZE is too small.  It must be larger than 1."
#elif ((RBUF2_SIZE & (RBUF2_SIZE - 1)) != 0)
    #error "RBUF2_SIZE must be a power of 2."
#endif

#ifndef SYS_FREQ
    #error "SYS_FREQ not defined!"
#endif

struct buf_st_2 {
    U32 in; // Next In Index
    U32 out; // Next Out Index
    U8 buf [RBUF2_SIZE]; // Buffer
};

static struct buf_st_2 rbuf_2 = { 0, 0, };
#define SIO_RBUFLEN_2 ((U16)(rbuf_2.in - rbuf_2.out)) 

static struct buf_st_2 tbuf_2 = { 0, 0, };
#define SIO_TBUFLEN_2 ((U16)(tbuf_2.in - tbuf_2.out))

static U32 tx_restart_2 = 1; // NZ if TX restart is required


static U32 USART2_RecBytes = 0;
U32    halUSART2_getRecBytes (void)
{
    return USART2_RecBytes;
}

static U32 USART2_SendBytes = 0;
U32    halUSART2_getSendBytes (void)
{
    return USART2_SendBytes;
}

static U32 USART2_errCount = 0; // ������ ������� ������
U32    halUSART2_getErrors (void)
{
    return USART2_errCount;
}


void halUSART2_flush (void)
{
    tbuf_2.in = 0; // Clear com buffer indexes
    tbuf_2.out = 0;
    tx_restart_2 = 1;

    rbuf_2.in = 0;
    rbuf_2.out = 0;
}

//------------------------------------------------------------------------------
// �������� �����/��
//------------------------------------------------------------------------------
MSG halUSART2_sndS (U8 dat)
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct buf_st_2 *p = &tbuf_2;
    
    if (SIO_TBUFLEN_2 < TBUF2_SIZE)
        { // If the buffer is full, return an error value
        p->buf [p->in & (TBUF2_SIZE - 1)] = dat; // Add data to the transmit buffer.
        p->in++;
        if (tx_restart_2)
        { // If transmit interrupt is disabled, enable it
            tx_restart_2 = 0;
            resp = FUNCTION_RETURN_OK;
            USART2->CR1 |= USART_CR1_TXEIE; // enable TX interrupt
        }
    }

    return resp;
}


MSG halUSART2_sndM (U8 *pDat, U16 dataSize)
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct buf_st_2 *p = &tbuf_2;
    U16 i;
    
    if ((0 < dataSize) && (TBUF2_SIZE >= dataSize))
    { // ��� �� �� ���������, ���. �� ���
        if ((TBUF2_SIZE - SIO_TBUFLEN_2) < dataSize) // 
        { return resp; }
        if (SIO_TBUFLEN_2 >= TBUF2_SIZE) // If the buffer is full, return an error value
        { return resp; }
        for (i = 0; i < dataSize; i++) { // Add data to the transmit buffer.
            p->buf [p->in & (TBUF2_SIZE - 1)] = pDat[i]; 
            p->in++;
        }
        if (0 != tx_restart_2) { // If transmit interrupt is disabled, enable it                  
            tx_restart_2 = 0;
            USART2->CR1 |= USART_CR1_TXEIE; // enable TX interrupt
        }
        resp = FUNCTION_RETURN_OK;
    }

    return resp;
}


//------------------------------------------------------------------------------
// ����� �����/��
//------------------------------------------------------------------------------
MSG halUSART2_rcvS (U8 *pDat)
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct buf_st_2 *p = &rbuf_2;

    if (SIO_RBUFLEN_2 != 0)
    {
        *pDat = (p->buf [(p->out) & (RBUF2_SIZE - 1)]);
        p->out++;
        resp = FUNCTION_RETURN_OK;
    }
    
    return resp;
}


MSG halUSART2_rcvM (U8 *pDat, U16 dataSize)
{
    MSG resp = FUNCTION_RETURN_ERROR;
    struct buf_st_2 *p = &rbuf_2;
    U16 i;

    if (0 < dataSize)
    {
        if ((0 < SIO_RBUFLEN_2) && (SIO_RBUFLEN_2 >= dataSize))
        {
            for (i = 0; i < dataSize; i++) {
                pDat[i] = (p->buf [(p->out) & (RBUF2_SIZE - 1)]);
                p->out++;
            }
            resp = FUNCTION_RETURN_OK;
        }
    }
    
    return resp;
}


void halUSART2_setBaud (U32 baud)
{
    USART2->BRR = (SYS_FREQ / 2) / baud; // 2- APB1 divider ->36MHz max for 72MHz
    /*
    switch (baud)
    {
        case USART_BAUD_9600: baud_temp = 72000000 / (U32)9600; break;
        case USART_BAUD_19200: baud_temp = 72000000 / (U32)19200; break;
        case USART_BAUD_115200: baud_temp = 72000000 / (U32)115200; break;
        
        case USART_BAUD_110: USART_InitStructure.USART_BaudRate = 110; break;
        case USART_BAUD_300: USART_InitStructure.USART_BaudRate = 300; break;
        case USART_BAUD_600: USART_InitStructure.USART_BaudRate = 600; break;
        case USART_BAUD_1200: USART_InitStructure.USART_BaudRate = 1200; break;
        case USART_BAUD_2400: USART_InitStructure.USART_BaudRate = 2400; break;
        case USART_BAUD_4800: USART_InitStructure.USART_BaudRate = 4800; break;
        case USART_BAUD_9600: USART_InitStructure.USART_BaudRate = 9600; break;
        case USART_BAUD_19200: USART_InitStructure.USART_BaudRate = 19200; break;
        case USART_BAUD_14400: USART_InitStructure.USART_BaudRate = 14400; break;
        case USART_BAUD_28800: USART_InitStructure.USART_BaudRate = 28800; break;
        case USART_BAUD_57600: USART_InitStructure.USART_BaudRate = 57600; break;
        case USART_BAUD_115200: USART_InitStructure.USART_BaudRate = 115200; break;
        case USART_BAUD_230400: USART_InitStructure.USART_BaudRate = 230400; break;
        case USART_BAUD_460800: USART_InitStructure.USART_BaudRate = 460800; break;
        case USART_BAUD_921600: USART_InitStructure.USART_BaudRate = 921600; break;
       
        //default: USART_InitStructure.USART_BaudRate = 9600; break;
    }
    USART2->BRR = (U16)baud_temp; */
}


void halUSART2_init (U32 baud)
{
    //��������� ������������
    RCC->APB2ENR |=   (RCC_APB2ENR_IOPAEN); //������������ GPIO, �������������� ������� GPIO
    //������������ USART2
    RCC->APB1ENR |=   RCC_APB1ENR_USART2EN; 
    
    //USART_DeInit (USART2);
    
    //���������������� PORTA.2 ��� TX
    GPIOA->CRL &= ~GPIO_CRL_MODE2;   //�������� ������� MODE
    GPIOA->CRL &= ~GPIO_CRL_CNF2;    //�������� ������� CNF
    GPIOA->CRL |=  GPIO_CRL_MODE2;   //�����, 50MHz
    GPIOA->CRL |=  GPIO_CRL_CNF2_1;  //�������������� �������, �����������
    //���������������� PORTA.3 ��� RX
    GPIOA->CRL &= ~GPIO_CRL_MODE3;   //�������� ������� MODE
    GPIOA->CRL &= ~GPIO_CRL_CNF3;    //�������� ������� CNF
    GPIOA->CRL |=  GPIO_CRL_CNF3_1;  //���������� ����, �������� � �����
    GPIOA->BSRR =  GPIO_BSRR_BS3;    //�������� ������������� ��������

    //������� ������ ������
    //USART2->BRR   =   0x0341;                            //C������� ������ 9600 ���
    if ((110 > baud) && (921600 < baud))
    {
        baud = 9600;
    }
    halUSART2_setBaud (baud);
    
    USART2->CR1 &= ~USART_CR1_M;        //8 ��� ������
    USART2->CR2 &= ~USART_CR2_STOP;     //���������� ����-�����: 1
     
    //���������� �������
    USART2->CR1 |= USART_CR1_UE;        //��������� ������ USART2
    USART2->CR1 |= USART_CR1_TE;        //��������� �����������
    USART2->CR1 |= USART_CR1_RE;        //��������� ���������


    NVIC_EnableIRQ (USART2_IRQn);
    NVIC_SetPriority (USART2_IRQn, 6);
    
    USART2_SendBytes = 0;
    USART2_RecBytes = 0;
    USART2_errCount = 0;
    
    halUSART2_flush ();
    
    USART2->CR1 |= USART_CR1_RXNEIE;
}


void halUSART2_deinit (void)
{
    USART2->CR1 &= ~USART_CR1_UE;
    GPIOA->CRL &= ~(GPIO_CRL_MODE2 | GPIO_CRL_MODE3);   //�������� ������� MODE
    GPIOA->CRL &= ~(GPIO_CRL_CNF2 | GPIO_CRL_CNF2);    //�������� ������� CNF
}


//#include "stm32f10x_it.h"
/**
 * ����������
 */
void USART2_IRQHandler (void)
{ // ����� ���������� ��� �������� � ������
    volatile U16 fStatus;
    struct buf_st_2 *p;
    
    fStatus = USART2->SR;
    if ((USART_SR_TXE & fStatus) != 0)
    { // ���������, ������������� �� ���������� ������� ���������� ��������
        USART2_LED_INV; // ����������� ���������
        USART2->SR &= ~USART_SR_TXE; // clear interrupt
        p = &tbuf_2;
        if (p->in != p->out) {
            USART2->DR = (p->buf [p->out & (TBUF2_SIZE - 1)] & 0x1FF);
            p->out++;
            tx_restart_2 = 0;
            USART2_SendBytes++;
        } else {
            tx_restart_2 = 1;
            USART2->CR1 &= ~USART_CR1_TXEIE; // disable TX interrupt if nothing to send
        }
    }
    if ((USART_SR_RXNE & fStatus) != 0)
    {
        USART2_LED_INV; // ����������� ���������
        USART2->SR &= ~USART_SR_RXNE;
        p = &rbuf_2;
        if (((p->in - p->out) & ~(RBUF2_SIZE - 1)) == 0)
        {
            p->buf [p->in & (RBUF2_SIZE - 1)] = (USART2->DR & 0x1FF);
            p->in++;
            USART2_RecBytes++;
        } else { // ���� ����� ��������, �� �������� ���������� �� ��������� �����
            
        }
    }
    
    if ((USART_SR_NE & fStatus)  /*!<Noise Error Flag */
        || (USART_SR_FE & fStatus)  /*!<Framing Error */
        || (USART_SR_PE & fStatus)  /*!<Parity Error */
        || (USART_SR_ORE & fStatus)) { // /*!<OverRun Error */
    //USART_ClearITPendingBit(USARTX, USART_IT_ORE);
        USART2_errCount++;
    }
}

#endif
