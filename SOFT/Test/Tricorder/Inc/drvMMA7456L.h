/**
 * @file    drvMMA7456L.h
 * @author  Ht3h5793
 * @date    
 * @version V1.0.0
 * @brief 



 */
 

#ifndef DRVMMA7456L_H
#define	DRVMMA7456L_H 20160409

#include "board.h"


#ifdef	__cplusplus
extern "C" {
#endif
    
MSG drvMMA7456L_testConnection (void);
     

#ifdef	__cplusplus
}
#endif

#endif	/** DRVMPU6050_H */
