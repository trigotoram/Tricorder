/**
 * @file    halUART.h
 * @author  Ht3h5793
 * @date    03.03.2014
 * @version V11.5.2
 * @brief   ������  ������� ������ ����� � �����������
            ��������� �������
            
    //�������� x �� �����        
    halUSARTx_init ();
    halUSARTx_flush ();
    U8 tmp_char = 0;
    while (1)
    {
        if (FUNCTION_RETURN_OK == halUSARTx_rcvS (&tmp_char))
        {
            halUSARTx_sndS (tmp_char);
        }
    }
*/

#ifndef HALUSART_H
#define	HALUSART_H 20150810

#include "board.h"


/**
 * ��� �������� ������ �� UART
 * ���� ��������� �� ������ ��������! ��� ������������� �� ��������� (����������, ����������, ��������� ������� �����) - 
 * ��������� �������� ������������� ������� hal_UART_SpeedTune, ������������ � ����� hal_UART_Protected
      * Speed must be one of 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600,
     * 19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000,
     * 1500000, 2000000, 2500000, 3000000, 3500000, 4000000
     
     */
typedef enum {
    USART_BAUD_110 = 110,
    USART_BAUD_300 = 300,
    USART_BAUD_600 = 600,
    USART_BAUD_1200 = 1200,
    USART_BAUD_2400 = 2400,
    USART_BAUD_4800 = 4800,
    USART_BAUD_9600 = 9600,
    USART_BAUD_14400 = 14400,
    USART_BAUD_19200 = 19200,
    USART_BAUD_28800 = 28800,
    USART_BAUD_38400 = 38400,
    USART_BAUD_56000 = 56000,
    USART_BAUD_57600 = 57600,
    USART_BAUD_115200 = 115200, /** 115200 ���/� */
    USART_BAUD_230400 = 230400,
    USART_BAUD_460800 = 460800,
    USART_BAUD_921600 = 921600,
    UART_SPEED_TOTAL_NUMBER, /** ����� ����� �������������� ��������� */
} USART_BAUD_type;


/**
 * ��� ���� �������� �������� ��� ������ �� UART
 */
typedef enum {
    _USART_PARITY_NONE = 0, /** ��� �������� �������� */
    _USART_PARITY_EVEN, /** �������� �� �������� */
    _USART_PARITY_ODD, /** �������� �� ���������� */
} UART_PARITY_type;

/**
 * ��� ����� ���� ��� ��� ������ �� UART
 */
typedef enum {
    USART_STOP_BITS_NUMBER_1 = 0, /** 1 ���� ��� */
    USART_STOP_BITS_NUMBER_2, /** 2 ���� ���� */
    USART_STOP_BITS_NUMBER_1_5, /** 1,5 ���� ��� */
} UART_STOP_BITS_NUMBER_type;

/**
 * ����� ����� ������ ��� ������ �� UART
 */
typedef enum {
    USART_WORD_LENGTH_5 = 0, /** 5 ��� */
    USART_WORD_LENGTH_6, /** 6 ��� */
    USART_WORD_LENGTH_7, /** 7 ��� */
    USART_WORD_LENGTH_8, /** 8 ��� */
    
} USART_WORD_LENGHT_type;


#ifdef	__cplusplus
extern "C" {
#endif

void    halUSART1_init (U32 baud);
void    halUSART1_deinit (void);
void    halUSART1_setBaud (U32 baud);
void    halUSART1_flush (void); //@todo ������� ������� ���� �������!
// �������� �����
MSG     halUSART1_sndS (U8 byte);
MSG     halUSART1_sndM (U8 *p_buf, U16 size); // ������� ���������� ���� � UART
// ����� �����
MSG     halUSART1_rcvS (U8 *p_byte);
MSG     halUSART1_rcvM (U8 *p_buf, U16 size);

// ����������� ���������
U32     halUSART1_getRecBytes (void);
U32     halUSART1_getSendBytes (void);
U32     halUSART1_getErrors (void);
    
//-------- USART2 -------------
//void      halUSART2_IRQ (void);
void    halUSART2_init (U32 baud);
void    halUSART2_deinit (void);
void    halUSART2_setBaud (U32 baud);
void    halUSART2_flush (void); //@todo ������� ������� ���� �������!
// �������� �����
MSG     halUSART2_sndS (U8 byte);
MSG     halUSART2_sndM (U8 *p_buf, U16 size); // ������� ���������� ���� � UART
// ����� �����
MSG     halUSART2_rcvS (U8 *p_byte);
MSG     halUSART2_rcvM (U8 *p_buf, U16 size);

// ����������� ���������
U32     halUSART2_getRecBytes (void);
U32     halUSART2_getSendBytes (void);
U32     halUSART2_getErrors (void);

//------- USART3 -------------
//void      halUSART3_IRQ (void);
void    halUSART3_init (U32 baud);
void    halUSART3_setBaud (U32 baud);
void    halUSART3_flush (void); //@todo ������� ������� ���� �������!
// �������� �����
MSG     halUSART3_sndS (U8 byte);
MSG     halUSART3_sndM (U8 *p_buf, U16 size); // ������� ���������� ���� � UART
// ����� �����
MSG     halUSART3_rcvS (U8 *p_byte);
MSG     halUSART3_rcvM (U8 *p_buf, U16 size);

// ����������� ���������
U32     halUSART3_getRecBytes (void);
U32     halUSART3_getSendBytes (void);
U32     halUSART3_getErrors (void);


//------- USART4 -------------
//void      halUSART3_IRQ (void);
void    halUSART4_init (U32 baud);
void    halUSART4_setBaud (U32 baud);
void    halUSART4_flush (void); //@todo ������� ������� ���� �������!
// �������� �����
MSG     halUSART4_sndS (U8 byte);
MSG     halUSART4_sndM (U8 *p_buf, U16 size); // ������� ���������� ���� � UART
// ����� �����
MSG     halUSART4_rcvS (U8 *p_byte);
MSG     halUSART4_rcvM (U8 *p_buf, U16 size);

// ����������� ���������
U32     halUSART4_getRecBytes (void);
U32     halUSART4_getSendBytes (void);
U32     halUSART4_getErrors (void);


#ifdef	__cplusplus
}
#endif

#endif	/* HALUSART_H */
