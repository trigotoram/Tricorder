/**
 * @file    modKey_local.h
 * @author  Ht3h5793, HTL1
 * @date    16.11.2016
 * @version V0.0.1
 * @brief   ��� ����� ��������� ������
 */

#ifndef MODKEY_LOCAL_H
#define MODKEY_LOCAL_H 20170807

#include "board.h"

typedef enum KEY_BUTTONS_ { // ���������� ������
    KEY_UP = 0,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_OK,
    KEY_MODE,
    KEY_SOUND,
    KEY_ON,
    
    NUMBER_KEYS         // ���������� ������, �� �������!
} KEY_BUTTONS;


/** ����������� �������� �������� ������, 
 *  ������ ������ 0!
 */
#define HALKEY_COUNTER_V_MIN            2

/** ����������� �������� �������� ������ 
 * ������� � ���, ��� ������������ �������� ���������� ����������!
*/
#define HALKEY_COUNTER_V_MAX            (HALKEY_COUNTER_V_MIN + 28)

/** �������� �������� ��� ������� ��������� ��� ������ ������ */
#define MODKEY_COUNTER_VALUE_ON         ((HALKEY_COUNTER_V_MAX * 90) / 100)   // 90% �� ������������� ��������
/** �������� �������� ��� ������� ��������� ��� ������ �������� */
#define MODKEY_COUNTER_VALUE_OFF        ((HALKEY_COUNTER_V_MAX * 50) / 100)  //

/** ����� �� ���������  ��������� ������, ��*/
#define MODKEY_DEFAULT_HOLD_TIME_PERIOD 1000
/** ����� �� ���������  ������ ������, ���� ����� �� �������, ��*/
#define MODKEY_DEFAULT_REALISED_TIMEOUT 6000


#endif /* MODKEY_LOCAL_H */
