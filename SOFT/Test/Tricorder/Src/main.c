/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
#include "board.h"
#include "_fifo.h"
#include "_crc.h"
#include "_debug.h"
#include "conv.h"
#include "xprintf.h"
#include "halI2C.h"
#include "halUSART.h"

#include "modRandom.h"
#include "modSysClock.h"
#include "halKey.h"
#include "modKey.h"
#include "colors.h"
#include "halPaint.h"
#include "modPaint.h"
#include "drvSPIFLASH.h"

#include "drvNRF24L01P.h"
#include "drvRFM69.h"

#include "drvMMA7456L.h"
#include "drvBH1750FVI.h"
#include "drvHMC5883L.h"
#include "drvBMP180.h"
#include "drvMPU6050.h"
#include "drvSHT21.h"
#include "modGPS.h"

#include "game_worm.h"


/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2S_HandleTypeDef hi2s3;
DMA_HandleTypeDef hdma_spi3_tx;

SD_HandleTypeDef hsd;
HAL_SD_CardInfoTypedef SDCardInfo;

osThreadId defaultTaskHandle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
char _str[64];
U32 i, j;
U32 size;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SDIO_SD_Init(void);
static void MX_I2S3_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void halADC_init (void)
{
    //__HAL_RCC_GPIOA_CLK_ENABLE();
    //__HAL_RCC_ADC1_CLK_ENABLE();
    RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_ADC1EN);
    
    /**ADC1 GPIO Configuration    
    PA1     ------> ADC1_IN1 
    */
    GPIOC->CRL &= ~GPIO_CRL_MODE1; //clean MODE
    GPIOC->CRL &= ~GPIO_CRL_CNF1; //clean CNF
    
    // TODO add calib?
	ADC1->CR2 |= ADC_CR2_ADON;              // 
	ADC1->CR2 |= ADC_CR2_EXTSEL;    // ������ �������������� �� ��������� ���� swstart
	ADC1->CR2 |= ADC_CR2_EXTTRIG;   // start software start
    ADC1->SMPR1 = 0;
    ADC1->SMPR2 = 0;
	ADC1->SMPR2 |= ADC_SMPR2_SMP1; // max cuircles (239.5) for 1 channel
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;
	ADC1->SQR3 |= 1; //ADC_SQR3_SQ1_4;    // �������� 1 ����� ��� 1 �������������� (���-�� �������������� ��-��������� 1)
}


void halADC_deinit (void)
{
    /* Peripheral clock disable */
    //__HAL_RCC_ADC2_CLK_DISABLE();
    RCC->APB2ENR &= ~RCC_APB2ENR_ADC1EN;
  
    /**ADC2 GPIO Configuration    
    PC2     ------> ADC2_IN12
    PC3     ------> ADC2_IN13 
    */
}


U16 halADC_get_BATT_mV (void)
{
    U32 i;
    U32 adc_val = 0;
    
    for (i = 0; i < 16; i++)
    {
        ADC1->CR2 |= ADC_CR2_SWSTART; // start conversion
        while (!(ADC1->SR & ADC_SR_EOC)) {}; // waiting end of conversion
        adc_val += (U32)ADC1->DR;
    }
    adc_val = (((adc_val * 2500) / 4096) * 2) / 16;
    
    return (U16)adc_val;
}


//SPI_HandleTypeDef hspi2;
void halSPI2_init (void)
{
    //GPIO_InitTypeDef GPIO_InitStruct;
    //__HAL_RCC_GPIOB_CLK_ENABLE();
    RCC->APB2ENR |= (RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN);
    
    /* Peripheral clock enable */
    //__HAL_RCC_SPI2_CLK_ENABLE();
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

    /**SPI2 GPIO Configuration    
    PB13     ------> SPI2_SCK
    PB14     ------> SPI2_MISO
    PB15     ------> SPI2_MOSI 
    */
    GPIOB->CRH &= ~(GPIO_CRH_MODE13 | GPIO_CRH_MODE14 | GPIO_CRH_MODE15);   //clean MODE
    GPIOB->CRH &= ~(GPIO_CRH_CNF13 | GPIO_CRH_CNF14 | GPIO_CRH_CNF15);    //clean CNF
    GPIOB->CRH |=  (GPIO_CRH_MODE13 | GPIO_CRH_MODE15);   //out, 50MHz
    GPIOB->CRH |=  (GPIO_CRH_CNF13_1 | GPIO_CRH_CNF15_1);  //symmetric, push-pull
    GPIOB->CRH |=  (GPIO_CRH_CNF14_1); //pull-up input
    GPIOB->ODR |=  GPIO_PIN_14;
    
    /*��������� SPI2 (Master)
    8 ��� ������, MSB ���������� ������, ����������� ����� ���������� NSS
    ����� NSS (PB12) ��������� ������������ � �������� ������*/
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN; //������������ ������ SPI2
    SPI2->CR1 |= SPI_CR1_BR; //Baud rate = Fpclk/256
    SPI2->CR1 &= ~SPI_CR1_DFF; //8 ��� ������
    SPI2->CR1 &= ~SPI_CR1_CPOL; //���������� ��������� �������
    SPI2->CR1 &= ~SPI_CR1_CPHA; //���� ��������� �������
    SPI2->CR1 &= ~SPI_CR1_LSBFIRST; //MSB ���������� ������
    SPI2->CR1 |= SPI_CR1_SSM; //����������� ����� NSS
    SPI2->CR1 |= SPI_CR1_SSI; //���������� ���������, ����� �� ����� NSS ������� �������
    //SPI2->CR2 |= SPI_CR2_SSOE; //����� NSS - ����� ���������� slave select
    SPI2->CR1 |= SPI_CR1_MSTR; //����� Master
    SPI2->CR1 |= SPI_CR1_SPE; //�������� SPI2
}


void halSPI2_deinit (void)
{
    /* Peripheral clock disable */
    //__HAL_RCC_SPI2_CLK_DISABLE();
    RCC->APB1RSTR |= RCC_APB1RSTR_SPI2RST;
    RCC->APB1RSTR &= ~RCC_APB1RSTR_SPI2RST;
    RCC->APB1ENR &= ~RCC_APB1ENR_SPI2EN;

    /**SPI2 GPIO Configuration    
    PB13     ------> SPI2_SCK
    PB14     ------> SPI2_MISO
    PB15     ------> SPI2_MOSI 
    */
    GPIOB->CRH &= ~(GPIO_CRH_MODE13 | GPIO_CRH_MODE14 | GPIO_CRH_MODE15); //clean MODE
    GPIOB->CRH &= ~(GPIO_CRH_CNF13 | GPIO_CRH_CNF14 | GPIO_CRH_CNF15); //clean CNF
}


U8 halSPI2_xput (U8 byte)
{
    while (0 == (SPI2->SR & SPI_SR_TXE)) {};
    SPI2->DR = byte;
    while (!(SPI2->SR & SPI_SR_RXNE)) {}; //TODO add mutex
    return SPI2->DR;
}


void drvBUZZER_peep (U8 numbers)
{
    RCC->APB2ENR |= (RCC_APB2ENR_IOPCEN | RCC_APB2ENR_AFIOEN);
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
    //__HAL_RCC_TIM8_CLK_ENABLE();

    GPIOC->CRL &= ~GPIO_CRL_MODE6; //clean MODE
    GPIOC->CRL &= ~GPIO_CRL_CNF6; //clean CNF
    GPIOC->CRL |= GPIO_CRL_MODE6; //out, symmetric 50MHz
    GPIOC->CRL |= GPIO_CRL_CNF6_1; //alternativ
    
    // ����������� ���������
    TIM3->PSC = 72;

    // ����������� ������ ������� = 1000 ������
    TIM3->ARR = 1000;

    // ����������� ���������� = 200 ������
    TIM3->CCR1 = 500;
    
    // ��������� 1 ����� � ����� ���2
    TIM3->CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_0;
    TIM3->CR1 |= TIM_CR1_ARPE; //autoreload

    // ����������� ������ �� ������������� 1 ������
    TIM3->CCER |= TIM_CCER_CC1E;
    
    while (numbers)
    {
        TIM3->CR1 |= TIM_CR1_CEN; //drvBUZZER_on (); //end init pika
        HAL_Delay (40);
        TIM3->CR1 &= ~TIM_CR1_CEN; //drvBUZZER_off ();
        HAL_Delay (90);
        numbers--;
    }
    //__HAL_RCC_TIM8_CLK_DISABLE();
    RCC->APB1ENR &= ~RCC_APB1ENR_TIM3EN;
    GPIOC->CRL &= ~GPIO_CRL_MODE6; //clean MODE
    GPIOC->CRL &= ~GPIO_CRL_CNF6; //clean CNF
}


void halUSART2_channel_select (U8 sel)
{
    switch (sel)
    {
    case UART2_SELECT_GSM:
        S0_GPIO_L;
        S1_GPIO_L;
        break;
        
    case UART2_SELECT_GPS:
        S0_GPIO_H;
        S1_GPIO_L;
        break;
        
    case UART2_SELECT_BT:
        S0_GPIO_L;
        S1_GPIO_H;
        break;
        
    case UART2_SELECT_RS485:
        S0_GPIO_H;
        S1_GPIO_H;
        break;
        
    default: S1_GPIO_L; S1_GPIO_L; break; //GSM
    }
    HAL_Delay (1); //TODO?
}

//TODO ADD go_to_sleep_mode




void  halRTC_init (void)                                                                            //������������� RTC
{
    if ((RCC->BDCR & RCC_BDCR_RTCEN) != RCC_BDCR_RTCEN)                 //�������� ������ �����, ���� �� ��������, �� ����������������
    {
        RCC->APB1ENR |= RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN;  //�������� ������������ PWR � Backup
        PWR->CR |= PWR_CR_DBP;                                                            //��������� ������ � Backup �������
        RCC->BDCR |= RCC_BDCR_BDRST;                                               //�������� Backup �������
        RCC->BDCR &= ~RCC_BDCR_BDRST;
        RCC->BDCR |= RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL_LSE;        //������� LSE �������� (����� 32768) � ������ ������������
        RCC->BDCR |= RCC_BDCR_LSEON;                                                //�������� LSE
        while ((RCC->BDCR & RCC_BDCR_LSEON) != RCC_BDCR_LSEON){} //��������� ���������
        BKP->RTCCR |= 3;                                                                         //���������� RTC
        while (!(RTC->CRL & RTC_CRL_RTOFF));                                         //��������� ��������� �� ��������� ��������� RTC
        RTC->CRL  |=  RTC_CRL_CNF;                                                        //��������� ������ � �������� RTC
        RTC->PRLL  = 0x7FFF;                                                                    //�������� �������� �� 32768 (32767+1)
        RTC->CRL  &=  ~RTC_CRL_CNF;                                                     //��������� ������ � �������� RTC
        while (!(RTC->CRL & RTC_CRL_RTOFF)) {};                                         //��������� ��������� ������
        RTC->CRL &= (uint16_t)~RTC_CRL_RSF;                                         //���������������� RTC
        while ((RTC->CRL & RTC_CRL_RSF) != RTC_CRL_RSF){};                 //��������� �������������
        PWR->CR &= ~PWR_CR_DBP;                                                         //��������� ������ � Backup �������
    }
}

 
U32 halRTC_get (void)                                                             //�������� �������� ��������
{
    return  (uint32_t)((RTC->CNTH << 16) | RTC->CNTL);
}


void halRTC_set (U32 count)                                                    //�������� ����� �������� ��������
{
    RCC->APB1ENR |= RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN;  //�������� ������������ PWR � Backup
    PWR->CR |= PWR_CR_DBP;                                                            //��������� ������ � Backup �������
    while (!(RTC->CRL & RTC_CRL_RTOFF)) {};                                         //��������� ��������� �� ��������� ��������� RTC
    RTC->CRL |= RTC_CRL_CNF;                                                          //��������� ������ � �������� RTC
    RTC->CNTH = count >> 16;                                                              //�������� ����� �������� �������� ��������
    RTC->CNTL = count;
    RTC->CRL &= ~RTC_CRL_CNF;                                                       //��������� ������ � �������� RTC
    while (!(RTC->CRL & RTC_CRL_RTOFF)) {};                                         //��������� ��������� ������
    PWR->CR &= ~PWR_CR_DBP;                                                         //��������� ������ � Backup �������
}






U8 GSM_buf[64];


FATFS SDFatFs;  /* File system object for SD card logical drive */
FIL MyFile;     /* File object */
char SDPath[4]; /* SD card logical drive path */

FRESULT res;                                          /* FatFs function common result code */
uint32_t byteswritten, bytesread;                     /* File write/read counts */
uint8_t wtext[] = "This is STM32 working with FatFs"; /* File write buffer */
uint8_t rtext[100];                                   /* File read buffer */

modGPS_RMC_s gpsRMC;
modGPS_GGA_s gpsGGA;
  
SYSTIME delay_draw;

S16 BMP180_S1[32];
S16 BMP180_S2[32];


/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
    //ZAY+++
    //remap must bee there
    RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
    AFIO->MAPR |= AFIO_MAPR_USART3_REMAP_FULLREMAP; //need do this only here, bks GPIO init early next 
    AFIO->MAPR |= AFIO_MAPR_TIM3_REMAP_FULLREMAP
        ;
    //ticker init ---------------------
    modSysClock_setRunPeriod (1000); // ����� ������� � �������
    SYSTIME _delay = modSysClock_getTime();
    //keys init
    modKey_init();
    _srand ();
    //ZAY---
    
    
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SDIO_SD_Init();
  MX_I2S3_Init();

  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2S3|RCC_PERIPHCLK_USB;
  PeriphClkInit.I2s3ClockSelection = RCC_I2S3CLKSOURCE_SYSCLK;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/8000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK_DIV8);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* I2S3 init function */
static void MX_I2S3_Init(void)
{

  hi2s3.Instance = SPI3;
  hi2s3.Init.Mode = I2S_MODE_MASTER_TX;
  hi2s3.Init.Standard = I2S_STANDARD_PHILIPS;
  hi2s3.Init.DataFormat = I2S_DATAFORMAT_16B_EXTENDED;
  hi2s3.Init.MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
  hi2s3.Init.AudioFreq = I2S_AUDIOFREQ_48K;
  hi2s3.Init.CPOL = I2S_CPOL_LOW;
  if (HAL_I2S_Init(&hi2s3) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SDIO init function */
static void MX_SDIO_SD_Init(void)
{

  hsd.Instance = SDIO;
  hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
  hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
  hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
  hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
  hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd.Init.ClockDiv = 6;

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel2_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel2_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through 
        * the Code Generation settings)
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pins : GPIO_Input_KEY_RIGHT_Pin GPIO_Input_KEY_DOWN_Pin GPIO_Input_KEY_MODE_Pin GPIO_Input_KEY_SOUND_Pin 
                           GPIO_Input_KEY_OK_Pin GPIO_Input_KEY_LEFT_Pin GPIO_Input_KEY_UP_Pin */
  GPIO_InitStruct.Pin = GPIO_Input_KEY_RIGHT_Pin|GPIO_Input_KEY_DOWN_Pin|GPIO_Input_KEY_MODE_Pin|GPIO_Input_KEY_SOUND_Pin 
                          |GPIO_Input_KEY_OK_Pin|GPIO_Input_KEY_LEFT_Pin|GPIO_Input_KEY_UP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Input_KEY_ON_Pin */
  GPIO_InitStruct.Pin = GPIO_Input_KEY_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIO_Input_KEY_ON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC1 PC2 PC3 
                           PC4 PC6 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3 
                           PA4 PA5 PA6 PA7 
                           PA9 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7 
                          |GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Out_S0_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_S0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIO_Out_S0_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_Out_S1_Pin GPIO_Out_BT_RES_Pin GPIO_Output_BT_MODE_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_S1_Pin|GPIO_Out_BT_RES_Pin|GPIO_Output_BT_MODE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Out_GPS_ON_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_GPS_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIO_Out_GPS_ON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_Out_WIFI_ON_Pin GPIO_Out_PWRVDD_ON_Pin GPIO_Output_RFM69_CS_Pin GPIO_Out_RFM69_RES_Pin 
                           GPIO_Output_NRF24_CS_Pin GPIO_Output_NRF24_CE_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_WIFI_ON_Pin|GPIO_Out_PWRVDD_ON_Pin|GPIO_Output_RFM69_CS_Pin|GPIO_Out_RFM69_RES_Pin 
                          |GPIO_Output_NRF24_CS_Pin|GPIO_Output_NRF24_CE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_Input_RFM69_IRQ_Pin GPIO_Input_NRF24_IRQ_Pin */
  GPIO_InitStruct.Pin = GPIO_Input_RFM69_IRQ_Pin|GPIO_Input_NRF24_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PB10 PB11 PB13 PB14 
                           PB15 PB6 PB7 PB8 
                           PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_13|GPIO_PIN_14 
                          |GPIO_PIN_15|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8 
                          |GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Output_SPIFLASH_CS_Pin */
  GPIO_InitStruct.Pin = GPIO_Output_SPIFLASH_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIO_Output_SPIFLASH_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PD8 PD9 PD10 PD11 
                           PD12 PD15 PD1 PD6 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12|GPIO_PIN_15|GPIO_PIN_1|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Out_GSM_ON_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_GSM_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIO_Out_GSM_ON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Out_LCD_ON_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_LCD_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIO_Out_LCD_ON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Out_VIBRO_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_VIBRO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIO_Out_VIBRO_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_Out_USB_RENUM_Pin GPIO_Out_LCD_CS_Pin GPIO_Out_LCD_RES_Pin GPIO_Out_LCD_RS_Pin 
                           GPIO_Out_SENSOR_ON_Pin */
  GPIO_InitStruct.Pin = GPIO_Out_USB_RENUM_Pin|GPIO_Out_LCD_CS_Pin|GPIO_Out_LCD_RES_Pin|GPIO_Out_LCD_RS_Pin 
                          |GPIO_Out_SENSOR_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_Input_I2C_IRQ_Pin */
  GPIO_InitStruct.Pin = GPIO_Input_I2C_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIO_Input_I2C_IRQ_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_Out_S0_GPIO_Port, GPIO_Out_S0_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_Out_S1_Pin|GPIO_Out_BT_RES_Pin|GPIO_Output_BT_MODE_Pin|GPIO_Output_SPIFLASH_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_Out_GPS_ON_GPIO_Port, GPIO_Out_GPS_ON_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_Out_WIFI_ON_Pin|GPIO_Out_PWRVDD_ON_Pin|GPIO_Output_RFM69_CS_Pin|GPIO_Out_RFM69_RES_Pin 
                          |GPIO_Output_NRF24_CS_Pin|GPIO_Output_NRF24_CE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_Out_GSM_ON_Pin|GPIO_Out_LCD_ON_Pin|GPIO_Out_LCD_CS_Pin|GPIO_Out_LCD_RES_Pin 
                          |GPIO_Out_LCD_RS_Pin|GPIO_Out_SENSOR_ON_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_Out_VIBRO_GPIO_Port, GPIO_Out_VIBRO_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_Out_USB_RENUM_GPIO_Port, GPIO_Out_USB_RENUM_Pin, GPIO_PIN_SET);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();

  /* init code for FATFS */
  MX_FATFS_Init();

  /* USER CODE BEGIN 5 */
ghgh:
    

    //GPIO 2 default
    GPIO_VIBRO_L;
    BT_GPIO_RES_L; //BT off
    BT_GPIO_MODE_L;
    GPIO_GSM_ON_L; //GSM off
    
    
    RFM69_GPIO_CS_H;
    RFM69_GPIO_RES_L;
    SPIFLASH_GPIO_CS_H;
    NRF24L01P_GPIO_CE_L;
    NRF24L01P_GPIO_CSN_H;
    
    //ON 3.3V DCDC ---------------------
    GPIO_PWRVDD_ON_H;
    HAL_Delay (50);
    
    //init common SPI2
    halSPI2_init ();
    
    //USARTs
    halUSART1_init (USART_BAUD_19200); //WIFI
    halUSART2_init (USART_BAUD_9600); //GSM
    
    __enable_irq();
    
    //test buzzer
    //drvBUZZER_peep (1);
    
    // on LCD backlight ----------------
    GPIO_LCD_ON_H;
    HAL_Delay (100);
    
    //init LCD -------------------------
    paint_init (SCREEN_ORIENTATION_GLOBAL);
    paint_setBackgroundColor (COLOR_BLACK);
    paint_clearScreen ();
    paint_setColor (COLOR_GREEN);
    paint_setFont (PAINT_FONT_Generic_8pt, PAINT_FONT_MS);

    //vibro test ------------------
    paint_putStrColRow (0, 0, "Vibro test...");
    GPIO_VIBRO_H;
    HAL_Delay (100);
    GPIO_VIBRO_L;
    HAL_Delay (200);
    GPIO_VIBRO_H;
    HAL_Delay (100);
    GPIO_VIBRO_L;
    
    // RTC init
    halRTC_init ();
    
    
    
    //ADC BATT -------------------------
    halADC_init();
    //halADC_get_BATT_mV();
    xsprintf (_str, "%u mV      ", halADC_get_BATT_mV());
    paint_putStrColRow (0, paint_getMaxRow() - 1, _str);
    HAL_Delay (150);

    // SD card test---------------------
    if (MSD_OK == BSP_SD_Init())
    {
        paint_putStrColRow (0, 0, "SD card present!");
    }
    
    //I2C sensors test -----------------
    paint_putStrColRow (0, 1, "I2C test start!");
    GPIO_SENSOR_ON_H;
    HAL_Delay (100);
    delay_draw = modSysClock_getTime();
#define I2C_SEARCH_SIZE 16
    volatile uint8_t I2C_search_buf[I2C_SEARCH_SIZE];
    volatile uint8_t I2C_search_cnt = 0;
    volatile uint8_t I2C_search_adress = 0;
    volatile uint8_t I2C_search_f = FALSE;
    U8 I2C_buf[4];
    for (i = 0; i < I2C_SEARCH_SIZE; i++) I2C_search_buf[i] = 0x00;
    halI2C_init ();
    /*
    if (FUNCTION_RETURN_OK == halI2C_re_init ())
    {
        paint_putStrColRow (0, 1, "I2C reinit!");
        HAL_Delay (100);
    }
    */
    while (1)
    {
        if (FALSE == I2C_search_f)
        {
            if (FUNCTION_RETURN_OK == halI2C_transmit(I2C_search_adress, &I2C_buf[0], 0, 0))
            {
                I2C_search_buf[I2C_search_cnt] = I2C_search_adress;
                char names[16];
                switch (I2C_search_adress){
                          case I2C_24Cxx_ADDRESS: strcpy (names, "24Cxx"); break;
                          case I2C_M24SR_ADDRESS: strcpy (names, "M24SR"); break;
                          case I2C_BMP180_ADDRESS: strcpy (names, "BMP180"); break;
                          case I2C_HMC5883L_ADDRESS: strcpy (names, "HMC5883L"); break;
                          case I2C_MPU6050_ADDRESS: strcpy (names, "MPU6050"); break;
                          case I2C_MMA7456L_ADDRESS: strcpy (names, "MMA7456L"); break;
                          case I2C_BH1750FVI_ADDRESS: strcpy (names, "BH1750FVI"); break;
                          case I2C_SHT21_ADDRESS: strcpy (names, "SHT21"); break;
                          default: strcpy (names, "?"); break;
                          }
                xsprintf (_str, "%02X - %s", I2C_search_adress, names);
                paint_putStrColRow (0, 3 + I2C_search_cnt, _str);
                I2C_search_cnt++;
            }
            I2C_search_adress += 2;
            xsprintf (_str, "%u", I2C_search_cnt);
            paint_putStrColRow (0, 2, _str);
            if (I2C_search_cnt >= I2C_SEARCH_SIZE)
            {
                I2C_search_f = TRUE;
                break;  
            }
            if (I2C_search_adress >= (0xFF-2))
            {
                I2C_search_f = TRUE;
                break;  
            }
        }
    }
    // acc test ------------------------
    if (FUNCTION_RETURN_OK == drvMMA7456L_testConnection ())
    {
        HAL_Delay (40);
    }
    if (FUNCTION_RETURN_OK == drvBMP180_Calibration(&BMP180_S1[0], &BMP180_S2[0]))
    {
        paint_setColor (COLOR_GREEN);
        paint_putStrColRow (0, 4 + I2C_search_cnt, "BMP180 - OK!");
    }
    
        
    paint_putStrColRow (0, 3 + I2C_search_cnt, "I2C test end.");
    HAL_Delay (800);
    paint_clearScreen ();
    
    
    //SPIFLASH test --------------------
    if (TRUE == drvSPIFLASH_init(0))
    {
        paint_setColor (COLOR_GREEN);
        paint_putStrColRow (0, 0, "SPIFLASH - OK!");
    }
    else
    {
        paint_setColor (COLOR_RED);
        paint_putStrColRow (0, 0, "SPIFLASH - ERROR!");
    }
    
    //NRF24L01P test -------------------
    nRF24_init();
    if (TRUE == nRF24_check())
    {
        paint_setColor (COLOR_GREEN);
        paint_putStrColRow (0, 1, "NRF24 - OK!");
    }
    else
    {
        paint_setColor (COLOR_RED);
        paint_putStrColRow (0, 1, "NRF24 - ERROR!");
    }

    //RFM69 test -----------------------
    if (TRUE == RFM69_init())
    {
        paint_setColor (COLOR_GREEN);
        paint_putStrColRow (0, 2, "RFM69 - OK!");
    }
    else
    {
        paint_setColor (COLOR_RED);
        paint_putStrColRow (0, 2, "RFM69 - ERROR!");
    }
    
    
    //GSM module test ------------------
    ////////////////////////////////////////////
    // !!! Not ON test, if ANT not connected!!!
    ////////////////////////////////////////////
    /*
    GPIO_GSM_ON_H; //GSM on
    halUSART2_setBaud (USART_BAUD_9600); //GSM
    halUSART2_flush ();
    halUSART2_channel_select (UART2_SELECT_GSM);
    HAL_Delay (100);
    while (1)
    {
        size = xsprintf (_str, "%s", "AT\r\n"); //test
        halUSART2_sndM ((U8*)_str, size);
        HAL_Delay (100);
        if (FUNCTION_RETURN_OK == halUSART2_rcvM (GSM_buf, 8))
        {
            HAL_Delay (10);
            break;
        }
    }
    
    //HAL_Delay (10000);
    //size = xsprintf (_str, "%s", "ATA\r\n"); //D+0954494621;
    //halUSART2_sndM ((U8*)_str, size);
     //ATD89161234567;
    
    GPIO_GSM_ON_L; //GSM off
    */
    
    // BT test -------------------------
    BT_GPIO_MODE_H;
    BT_GPIO_RES_H; //BT on
    halUSART2_setBaud (USART_BAUD_9600);
    halUSART2_flush ();
    halUSART2_channel_select (UART2_SELECT_BT);
    HAL_Delay (10);
    while (1)
    {
        size = xsprintf (_str, "%s", "AT+HELP\r\n"); //test
        halUSART2_sndM ((U8*)_str, size);
        HAL_Delay (100);
        if (FUNCTION_RETURN_OK == halUSART2_rcvM (GSM_buf, 8))
        {
            break;
        }
    }
    BT_GPIO_RES_L; //BT off
    BT_GPIO_MODE_L;
    
    //GPS module test ------------------
    GPIO_GPS_ON_L; //GPS on
    halUSART2_setBaud (USART_BAUD_115200);
    halUSART2_flush();
    halUSART2_channel_select (UART2_SELECT_GPS);
    HAL_Delay (50);
    paint_clearScreen ();
    paint_putStrColRow (0, 0, "GPS");
    modGPS_init (&gpsRMC);
    char tmp_char;
    BOOL GPS_status = FALSE;
    U32 test_cnt = 0;
    while (1)
    {
        if (FUNCTION_RETURN_OK == halUSART2_rcvS ((U8 *)&tmp_char))
        {
            modGPS_parseRMC (tmp_char);
            modGPS_parseGGA (tmp_char); //for altitude
            
            if ((0 != gpsRMC.Fix) && (0 != gpsGGA.Fix))
            {
                if (FALSE == GPS_status)
                {
                    GPS_status = TRUE;
                    drvBUZZER_peep (1);
                }
            }
            break;
        }
        
        // paint every 1 sec
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&delay_draw, 1000, SYSCLOCK_GET_TIME_MS_1))
        {
            S32 gps_LT, gps_LG;
            if ((0 != gpsRMC.Fix))// && (0 != gpsGGA.Fix))
            {
            	// converting
            	gps_LT = modGPS_dms2dd (gpsRMC.Lat);
            	gps_LG = modGPS_dms2dd (gpsRMC.Long);
                
                //test_cnt += 100; // for fast out
            }
            
            if (FALSE == GPS_status)
            {
                paint_setColor (COLOR_RED);
            }
            else
            {
                paint_setColor (COLOR_GREEN);
            }
            
            xsprintf (_str, "F:%u T:%u          ", gpsRMC.Fix, gpsRMC.Time);
            paint_putStrColRow (0, 1, _str);
            
            xsprintf (_str, "LNG: %d.%07u", gps_LG / 10000000, gps_LG % 10000000);
            paint_putStrColRow (0, 2, _str);
            xsprintf (_str, "LAT: %d.%07u", gps_LT / 10000000, gps_LT % 10000000);
            paint_putStrColRow (0, 3, _str);
            
            xsprintf (_str, "ALT: %d.%07u", gpsGGA.Altitude / 10);
            paint_putStrColRow (0, 4, _str);
            
            xsprintf (_str, "DATE: %u", gpsRMC.Date);// / 10000, (gpsRMC.Date / 100) % 100, gpsRMC.Date % 100);
            paint_putStrColRow (0, 5, _str);
            
            if (++test_cnt > (60 * 20)) //~20 minutes
            {
                //break;
            }
        }
        
    }
    GPIO_GPS_ON_H; //GPS off
    
    
    //WIFI module test -----------------
    GPIO_WIFI_ON_H; //WIFI on
    HAL_Delay (200); //for waiting flush trash
    halUSART1_flush();
    while (1)
    {
        size = xsprintf (_str, "%s", "AT+HELP\r\n"); //test
        halUSART1_sndM ((U8*)_str, size);
        HAL_Delay (100);
        if (FUNCTION_RETURN_OK == halUSART1_rcvM (GSM_buf, 8))
        {
            HAL_Delay (10);
            break;
        }
    }
    
    
    GPIO_WIFI_ON_L; //WIFI off
    

    
    game_worm_init ();
    while (1)
    {
        game_worm_run ();
    }
    
    //KEY test
    while (1)
    {
        modKey_run();
        

        if (FUNCTION_RETURN_OK == modSysClock_timeout (&delay_draw, 100, SYSCLOCK_GET_TIME_MS_1))
        {
            U8 keys = 0;
            KEY_STATE state;
            state = modKey_getState (KEY_UP);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            } keys = keys << 1;
            state = modKey_getState (KEY_DOWN);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            } keys = keys << 1;
            state = modKey_getState (KEY_LEFT);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            } keys = keys << 1;
            state = modKey_getState (KEY_RIGHT);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            } keys = keys << 1;
            state = modKey_getState (KEY_OK);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            } keys = keys << 1;
            state = modKey_getState (KEY_MODE);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            } keys = keys << 1;
            state = modKey_getState (KEY_SOUND);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1; break;
            } keys = keys << 1;
            state = modKey_getState (KEY_ON);
            if (state >= MODKEY_STATE_PRESSED)
            {
                keys = keys | 1;
            }
            
            xsprintf (_str, "    %08B", keys);
            paint_putStrColRow (0, 5, _str);
        }
        
    }
    
    
    //all off --------------------------
    
    
    //LCD off
    GPIO_LCD_ON_L;
    hal_paint_deinit(0);
    
    GPIO_SENSOR_ON_L;
    //need to gnd before to off VDD!
    RFM69_GPIO_RES_L;
    //all CS to GND
    RFM69_GPIO_CS_L;
    SPIFLASH_GPIO_CS_L;
    NRF24L01P_GPIO_CE_L;
    NRF24L01P_GPIO_CSN_L;
    BT_GPIO_RES_L;
    HAL_Delay (30);
    halSPI2_deinit ();
    halUSART1_deinit ();
    halUSART2_deinit ();
    HAL_Delay (10);
    // PWR off
    GPIO_PWRVDD_ON_L;
    HAL_Delay (10);
    
    
    /* Infinite loop */
    for(;;)
    {
        
        
        osDelay(100);
    }
  /* USER CODE END 5 */ 
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM5 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
/* USER CODE BEGIN Callback 0 */

/* USER CODE END Callback 0 */
  if (htim->Instance == TIM5) {
    HAL_IncTick();
  }
/* USER CODE BEGIN Callback 1 */
    modSysClock_run();
    halKey_run();

/* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
