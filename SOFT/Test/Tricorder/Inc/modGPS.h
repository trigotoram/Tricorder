/**
 * @file    halUART.h
 * @author  Ht3h5793
 * @date    19.07.2016
 * @version V1.0.0

*/

#ifndef MODGPS_H
#define	MODGPS_H 20160719

#include "board.h"


typedef struct modGPS_RMC_s_ {

	S16 state; // = 0;
	S32 temp;
	S32 ltmp;

	S32 Time, Msecs, Knots, Course, Date;
	S32 Lat, Long;
	BOOL Fix;
	BOOL Valide;
} modGPS_RMC_s;

typedef struct modGPS_GGA_s_ {

	S16 state; // = 0;
	S32 temp;
	S32 ltmp;

	BOOL Fix;
	S32 Altitude;
} modGPS_GGA_s;


#ifdef	__cplusplus
extern "C" {
#endif

/**
 *
 */
void    modGPS_init (modGPS_RMC_s *s);

/**
 *
 */
void    modGPS_parseRMC (char c);
void    modGPS_parseGGA (char c);


/**
 * Convert GPA data format from dd.mm.sssss to dd.dd.ddddd
 * All data is fixed point
 */
S32 modGPS_dms2dd (S32 val);


#ifdef	__cplusplus
}
#endif

#endif	/* MODGPS_H */
