#include "drvMMA7456L.h"
#include "board.h"
#include "halI2C.h"
#include "_debug.h"


#define MMA7456L_RA_WHO_AM_I         0x0F


/*
 * test connectino to chip
 */
MSG drvMMA7456L_testConnection (void)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[2];
    
    I2C_buf[0] = MMA7456L_RA_WHO_AM_I; // register WHO_AM_I
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_MMA7456L_ADDRESS, &I2C_buf[0], 1, 100))
    {
        I2C_buf[0] = 0x00;
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_MMA7456L_ADDRESS, &I2C_buf[0], 1, 100))
        {
            if (0 == I2C_buf[0])
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}