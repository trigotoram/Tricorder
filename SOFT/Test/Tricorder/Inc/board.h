/* 
 * File:   board.h
 */

#ifndef BOARD_H
#define	BOARD_H 20170712

#define BOARD_TRICORDER_vB              (1)//������� �����
#define STM32                           1//for debug.h

/**
 *  ������ ��� "include"
 */
#include "main.h"
#include "stm32f1xx_hal.h"
#include "usb_device.h"
//#include "usbd_cdc_if.h"
#include "conv.h"
#include "binary.h"


/**
 *  ������ ��� "define"
 */
// defines -----------------------------
#ifndef TRUE
    #define TRUE                        1
#endif
#ifndef FALSE
    #define FALSE                       0
#endif

#ifndef NULL
  #define NULL                          (0)
#endif

#define FUNCTION_RETURN_OK              1 //- �������� ���������
#define FUNCTION_RETURN_ERROR           0
#define U8                              uint8_t
#define S8                              int8_t
#define VU8                             uint8_t
#define U16                             uint16_t
#define S16                             int16_t
#define U32                             uint32_t
#define S32                             int32_t
#define U64                             uint64_t
#define S64                             int64_t
#define BOOL                            uint32_t

#define PROGMEM

#ifndef bool
#define bool                            u32
#endif
#ifndef false
#define false                           0
#endif
#ifndef true
#define true                            1
#endif
#define _DEBUG                          1 //commit, if not need debug

#ifndef MSG
    #define MSG                         uint32_t
#endif

//#define ERROR_ACTION(a)              //fERROR_ACTION(a,__MODULE__,__LINE__)

#define SYSTIME                         uint32_t
#define TIC_PERIOD                      (1000UL) //us
//#define CPU_FRQ                         (72000000UL) //us
#define SYS_FREQ                        (72000000UL)


//for modRandom.h ----------------------
#define MODRANDOM_4096BIT_EN            1

// for xprintf.h -----------------------
#define _USE_XFUNC_OUT	                1 // 1: Use output functions
#define	_CR_CRLF		                0 // 1: Convert \n ==> \r\n in the output char
#define _USE_XFUNC_IN	                0 // 1: Use input function
#define	_LINE_ECHO		                0 // 1: Echo back input chars in xgets function

//for conv.h
#define STR_MAX_SIZE                    (65535 -1)

// for modRTC.h ------------------------
//#define MOD_RTC                     1
//#define RTC_CORRECT_TIC             1
//#define RTC_NEED_CALENDAR           1
//#define RTC_NEED_ALARM              1

// for _crc.h --------------------------
//#define NEED_CRC32_CCITT        1 
#define NEED_CRC16                      1
//#define NEED_CRC8               1
//#define NEED_CRC8_DS            1

// CPU misc ----------------------------
//#define __NOP()               
#define __DI()                          __disable_irq() // do { #asm("cli") } while(0) // Global interrupt disable
#define __EI()                          __enable_irq() //do { #asm("sei") } while(0) // Global interrupt enable
#define __CLRWDT()                      do { IWDG->KR = 0x0000AAAA; } while (0) // ������������ �������


// GPIO --------------------------------
#define GPIO_KEY_LEFT                   (GPIOE->IDR & GPIO_Input_KEY_LEFT_Pin)// GPIO_PIN_0
#define GPIO_KEY_UP                     (GPIOE->IDR & GPIO_Input_KEY_UP_Pin) // GPIO_PIN_1
#define GPIO_KEY_RIGHT                  (GPIOE->IDR & GPIO_Input_KEY_RIGHT_Pin) //GPIO_PIN_2
#define GPIO_KEY_DOWN                   (GPIOE->IDR & GPIO_Input_KEY_DOWN_Pin) //GPIO_PIN_3
#define GPIO_KEY_MODE                   (GPIOE->IDR & GPIO_Input_KEY_MODE_Pin) //GPIO_PIN_4
#define GPIO_KEY_SOUND                  (GPIOE->IDR & GPIO_Input_KEY_SOUND_Pin) //GPIO_PIN_5
#define GPIO_KEY_OK                     (GPIOE->IDR & GPIO_Input_KEY_OK_Pin) //GPIO_PIN_6
#define GPIO_KEY_ON                     (GPIOC->IDR & GPIO_Input_KEY_ON_Pin) //GPIO_PIN_13

// POWER
#define GPIO_PWRVDD_ON_H                GPIOE->BSRR = GPIO_Out_PWRVDD_ON_Pin
#define GPIO_PWRVDD_ON_L                GPIOE->BSRR = (U32)GPIO_Out_PWRVDD_ON_Pin  << 16
#define GPIO_SENSOR_ON_H                GPIOD->BSRR = GPIO_Out_SENSOR_ON_Pin
#define GPIO_SENSOR_ON_L                GPIOD->BSRR = (U32)GPIO_Out_SENSOR_ON_Pin  << 16
#define GPIO_LCD_ON_H                   GPIOD->BSRR = GPIO_Out_LCD_ON_Pin
#define GPIO_LCD_ON_L                   GPIOD->BSRR = (U32)GPIO_Out_LCD_ON_Pin  << 16

// SPIFLASH
#define SPIFLASH_GPIO_CS_H              GPIOB->ODR |= GPIO_PIN_12
#define SPIFLASH_GPIO_CS_L              GPIOB->ODR &= ~GPIO_PIN_12

//RFM69 
#define RFM69_GPIO_CS_H                 GPIOE->ODR |= GPIO_PIN_10
#define RFM69_GPIO_CS_L                 GPIOE->ODR &= ~GPIO_PIN_10
#define RFM69_GPIO_RES_H                GPIOE->ODR |= GPIO_PIN_11
#define RFM69_GPIO_RES_L                GPIOE->ODR &= ~GPIO_PIN_11
#define RFM69_GPIO_IRQ_IN               (GPIOE->IDR & GPIO_PIN_12)
            
//NRF24
#define NRF24L01P_GPIO_CSN_H            GPIOE->ODR |= GPIO_PIN_13
#define NRF24L01P_GPIO_CSN_L            GPIOE->ODR &= ~GPIO_PIN_13
#define NRF24L01P_GPIO_CE_H             GPIOE->ODR |= GPIO_PIN_14
#define NRF24L01P_GPIO_CE_L             GPIOE->ODR &= ~GPIO_PIN_14
#define NRF24L01P_GPIO_IRQ_IN           (GPIOE->IDR & GPIO_PIN_15)

//BT
#define BT_GPIO_RES_H                   GPIOB->BSRR = GPIO_PIN_1
#define BT_GPIO_RES_L                   GPIOB->BSRR = (U32)GPIO_PIN_1 << 16;
#define BT_GPIO_MODE_H                  GPIOB->BSRR = GPIO_PIN_2
#define BT_GPIO_MODE_L                  GPIOB->BSRR = (U32)GPIO_PIN_2 << 16;        

// 74HC4052
enum { 
    UART2_SELECT_GSM = 0, 
    UART2_SELECT_BT,
    UART2_SELECT_GPS,
    UART2_SELECT_RS485
};

#define S1_GPIO_H                       GPIOB->BSRR = GPIO_PIN_0
#define S1_GPIO_L                       GPIOB->BSRR = (U32)GPIO_PIN_0  << 16; 
#define S0_GPIO_H                       GPIOC->BSRR = GPIO_PIN_5
#define S0_GPIO_L                       GPIOC->BSRR = (U32)GPIO_PIN_5  << 16; 

// GSM
#define GPIO_GSM_ON_H                   GPIOD->BSRR = GPIO_Out_GSM_ON_Pin
#define GPIO_GSM_ON_L                   GPIOD->BSRR = (U32)GPIO_Out_GSM_ON_Pin  << 16

// GPS
#define GPIO_GPS_ON_H                   GPIOE->BSRR = GPIO_Out_GPS_ON_Pin
#define GPIO_GPS_ON_L                   GPIOE->BSRR = (U32)GPIO_Out_GPS_ON_Pin  << 16

// WIFI
#define GPIO_WIFI_ON_H                  GPIOE->BSRR = GPIO_Out_WIFI_ON_Pin
#define GPIO_WIFI_ON_L                  GPIOE->BSRR = (U32)GPIO_Out_WIFI_ON_Pin  << 16

//VIBRO
#define GPIO_VIBRO_H                    GPIO_Out_VIBRO_GPIO_Port->BSRR = GPIO_Out_VIBRO_Pin
#define GPIO_VIBRO_L                    GPIO_Out_VIBRO_GPIO_Port->BSRR = (U32)GPIO_Out_VIBRO_Pin  << 16


// end GPIO ----------------------------

// LCD ---------------------------------
#define HT3_PAINT                       1
#define LCD_NOKIA1616                   1
#define LCD_LPH9157                     0
#define COLORS_16BIT                    1

enum { 
    SCREEN_ORIENTATION_0 = 0, 
    SCREEN_ORIENTATION_90,
    SCREEN_ORIENTATION_180,
    SCREEN_ORIENTATION_270
};

//--------------------------------------
#if (LCD_NOKIA1616)
    
#define COLORS_16BIT                    1
#if COLORS_16BIT
    typedef U16                         COLOR;
#else
    #define COLORS_8BIT                 1
    typedef U8                          COLOR;
#endif

#define LCD_UART                        1
#if LCD_UART
    #define LCD_DMA                     1
    #if LCD_DMA
        #define LCD_DMA_BUF_SIZE        128
    #endif
#endif


typedef uint32_t                        COORD;
    
#define PAINT_FONT_Generic_8pt          1
#define PAINT_NEED_LINE                 1
#define PAINT_NEED_RECT                 1
#define PAINT_NEED_CIRCLE               1

    #define SCREEN_W	                128
    #define SCREEN_H	                160
    #define SCREEN_VIRTUAL_W	        SCREEN_W
    #define SCREEN_VIRTUAL_H	        SCREEN_H
    #define SCREEN_ORIENTATION_GLOBAL   SCREEN_ORIENTATION_0 //LCD_ORIENTATION_180
    #if (SCREEN_W > SCREEN_H)
        #define PAINT_BUF_SIZE          SCREEN_W
    #else
        #define PAINT_BUF_SIZE          SCREEN_H
    #endif

    #define LCD_CTRL_X_OFFSET           2 // TODO ��� ������ ���������� �������� �������!!!
    #define LCD_CTRL_Y_OFFSET           1

    #define GPIO_LCD_RES                GPIOD
    #define GPIO_Pin_LCD_RES            GPIO_PIN_4
    #define GPIO_LCD_CS                 GPIOD
    #define GPIO_PIN_LCD_CS             GPIO_PIN_3

    #define GPIO_LCD_CLK                GPIOD
    #define GPIO_PIN_LCD_CLK            GPIO_PIN_10
    #define GPIO_LCD_DAT                GPIOD
    #define GPIO_PIN_LCD_DAT            GPIO_PIN_8

    #if LCD_UART
        #define LCD_CS_L                { GPIO_LCD_CS->BSRR = (uint32_t)GPIO_PIN_LCD_CS << 16; }
        #define LCD_CS_H                do{ while ((USART3->SR & USART_SR_TC) == 0){}; GPIO_LCD_CS->BSRR = GPIO_PIN_LCD_CS; } while(0)
    #else
        #define LCD_CLK_L               GPIO_LCD_CLK->BSRR = (uint32_t)GPIO_PIN_LCD_CLK << 16
        #define LCD_CLK_H               GPIO_LCD_CLK->BSRR = GPIO_PIN_LCD_CLK

        #define LCD_DAT_L               { GPIO_LCD_DAT->BSRR = (uint32_t)GPIO_PIN_LCD_DAT << 16; }
        #define LCD_DAT_H               { GPIO_LCD_DAT->BSRR = GPIO_PIN_LCD_DAT; } 
        #define LCD_DAT_IN                  (GPIOC->IDR & GPIO_PIN_LCD_DAT)

        #define LCD_CS_L                { GPIO_LCD_CS->BSRR = (uint32_t)GPIO_PIN_LCD_CS << 16; __NOP();}
        #define LCD_CS_H                { GPIO_LCD_CS->BSRR = GPIO_PIN_LCD_CS; __NOP();}
    #endif

    #define LCD_RES_L                   GPIO_LCD_RES->BSRR = (uint32_t)GPIO_Pin_LCD_RES << 16
    #define LCD_RES_H                   GPIO_LCD_RES->BSRR = GPIO_Pin_LCD_RES
    
#endif
//--------------------------------------
#if (LCD_LPH9157)

#ifndef COORD
typedef uint8_t                         COORD;
#endif

#ifndef COLOR
#if COLORS_16BIT
typedef uint16_t                        COLOR;
#else
typedef uint8_t                         COLOR;
#endif
#endif

//#define PAINT_FONT_x3y5                 1
#define PAINT_FONT_Generic_8pt          1
#define PAINT_NEED_LINE                 1
#define PAINT_NEED_RECT                 1
#define PAINT_NEED_CIRCLE               1

//#define BLACK 0
//#define WHITE 1
#define INVERSE 2

//#define LCD_ENABLE_PARTIAL_UPDATE   1

#define SCREEN_W	                    132
#define SCREEN_H	                    178
#define SCREEN_VIRTUAL_W                SCREEN_W
#define SCREEN_VIRTUAL_H                SCREEN_H

#define SCREEN_ORIENTATION_GLOBAL          SCREEN_ORIENTATION_180
#if (SCREEN_W > SCREEN_H)
    #define PAINT_BUF_SIZE              SCREEN_W
#else
    #define PAINT_BUF_SIZE              SCREEN_H
#endif

#define LCD_RES_L                       HAL_GPIO_WritePin(LCD_RES_GPIO_Port, LCD_RES_Pin, GPIO_PIN_RESET);
#define LCD_RES_H                       HAL_GPIO_WritePin(LCD_RES_GPIO_Port, LCD_RES_Pin, GPIO_PIN_SET);

#define LCD_CS_L                        GPIOB->ODR &= ~LCD_CS_Pin //HAL_GPIO_WritePin(GPIOB, LCD_CS_Pin, GPIO_PIN_RESET);
#define LCD_CS_H                        GPIOB->ODR |= LCD_CS_Pin //HAL_GPIO_WritePin(GPIOB, LCD_CS_Pin, GPIO_PIN_SET);

#define LCD_RS_L                        GPIOB->ODR &= ~LCD_RS_Pin //HAL_GPIO_WritePin(GPIOB, LCD_RS_Pin, GPIO_PIN_RESET);
#define LCD_RS_H                        GPIOB->ODR |= LCD_RS_Pin //HAL_GPIO_WritePin(GPIOB, LCD_RS_Pin, GPIO_PIN_SET);

#endif
// end LCD -----------------------------

// my colors ---------------------------
#if HT3_PAINT
    #define COLOR_NORMAL                COLOR_WHITE
    #define COLOR_WARNING               COLOR_BLUE
    #define COLOR_ERROR                 COLOR_RED
    #define COLOR_OK                    COLOR_GREEN
    #define COLOR_BACKGRAUND            COLOR_BLACK
#else
    #define COLOR_NORMAL                White
    #define COLOR_WARNING               Yellow
    #define COLOR_ERROR                 Red
    #define COLOR_OK                    Green
    #define COLOR_BACKGRAUND            Black
#endif

#define PAINT_POSX_1                    2
#define PAINT_POSX_2                    52
#define PAINT_POSX_3                    102

#define PAINT_POSY_1B                   8
#define PAINT_POSY_1T                   24
#define PAINT_POSY_2B                   48
#define PAINT_POSY_2T                   64

#define PAINT_POSY_3T                   102

#define BAR_WIGHT                       45 
#define BAR_HEIGHT                      12 
// end my colors -----------------------

// I2C ---------------------------------
#define HAL_I2C1                        1

#define I2C_BMP180                      1
#define I2C_HMC5883L                    1
#define I2C_MPU6050                     1
//#define I2C_MAX30100                    1

//������ �� ���������


#define I2C_HMC5883L_ADDRESS            0x1E

#define I2C_24Cxx_ADDRESS               0x50
#define I2C_M24SR_ADDRESS               0x56
#define I2C_BH1750FVI_ADDRESS           0x5C//0xB8
#define I2C_MPU6050_ADDRESS             0x68

#define I2C_MMA7456L_ADDRESS            0xDC //0x3A

//#define I2C_MAX30100_ADDRESS            0x57
//9E
//D0
//D6
//E8


#define I2C_BMP180_ADDRESS              0xEE
#define I2C_SHT21_ADDRESS               0x80

//#define I2C_LM75_ADRESS             0x4F
//#define SSD1306_I2C_ADDRESS         0x78  //0x3C // 011110+SA0+RW - 0x3C or 0x3D
//#define I2C_LM75_ADRESS 0x9E
    
#if (1 == HAL_I2C1)
    #define GPIO_I2C1                   GPIOB
    #define GPIO_PIN_SDA1               GPIO_PIN_11
    #define GPIO_PIN_SCL1               GPIO_PIN_10

    #define SCL1_L                      GPIO_I2C1->ODR &= ~GPIO_PIN_SCL1 //I2C_GPIO->ODR &= ~GPIO_Pin_SCL                                
    #define SCL1_H                      GPIO_I2C1->ODR |=  GPIO_PIN_SCL1 //I2C_GPIO->ODR |=  GPIO_Pin_SCL

    #define SDA1_L                      GPIO_I2C1->ODR &= ~GPIO_PIN_SDA1
    #define SDA1_H                      GPIO_I2C1->ODR |=  GPIO_PIN_SDA1

    #define SCL1_IN                     (GPIO_I2C1->IDR & GPIO_PIN_SCL1)
    #define SDA1_IN                     (GPIO_I2C1->IDR & GPIO_PIN_SDA1)

    // GPIO_Init(I2C_GPIO, (GPIO_Pin_TypeDef)GPIO_Pin_SDA, GPIO_MODE_OUT_OD_HIZ_FAST); \
    // GPIO_Init(I2C_GPIO, (GPIO_Pin_TypeDef)GPIO_Pin_SCL, GPIO_MODE_OUT_OD_HIZ_FAST); 
    #define I2C1_INIT        \
        GPIO_InitTypeDef   GPIO_InitStructure; \
        __GPIOB_CLK_ENABLE(); \
        GPIO_InitStructure.Pin   = GPIO_PIN_SDA1 | GPIO_PIN_SCL1; \
        GPIO_InitStructure.Mode  = GPIO_MODE_OUTPUT_OD; \
        GPIO_InitStructure.Speed = GPIO_SPEED_HIGH; \
        GPIO_InitStructure.Pull  = GPIO_PULLUP; \
        HAL_GPIO_Init (GPIO_I2C1, &GPIO_InitStructure)
        
    #define I2C1_DEINIT
    #define I2C1_DELAY                  halI2C1_delay()
            
#define halI2C_init()                   soft_halI2C_init() 
#define halI2C_transmit(a,b,c,d)        soft_halI2C_transmit(a,b,c,d) 
#define halI2C_receive(a,b,c,d)         soft_halI2C_receive(a,b,c,d) 

#endif


// end I2C -----------------------------


// UARTs -------------------------------
#define HAL_USART1                      1
#define USART1_TX_HOOK                  //do {Uart1TX_Ready = SET;} while (0)
#define USART1_RX_HOOK                  //do {Uart1RX_Ready = SET; ; } while (0)
#define USART1_TX_ERROR_HOOK            //ERROR_ACTION(ERROR_USART1_HAL)
#define USART1_TBUF_SIZE                (1024)
#define USART1_RBUF_SIZE                (1024)
#define USART1_LED_INV                  //LED_BLUE_INV 

#define HAL_USART2                      1
#define USART2_TX_HOOK                  //do {Uart2TX_Ready = SET;} while (0)
#define USART2_RX_HOOK                  //do {Uart2RX_Ready = SET; } while (0)
#define USART2_TX_ERROR_HOOK            //ERROR_ACTION(ERROR_USART2_HAL)
#define USART2_TBUF_SIZE                (1024)
#define USART2_RBUF_SIZE                (1024)
#define USART2_LED_INV                   

// end UARTs ---------------------------

// SPIs --------------------------------
#define HAL_SPI1                        1

#define halSPIFLASH_xspi                halSPI2_xput
#define halSPI_NRF24LC01P_xput          halSPI2_xput
#define halSPI_RFM69_xput               halSPI2_xput

// end SPIs ----------------------------      
            
// etc ---------------------------------
#define show_message(a)                 show_message_on_LCD(a) //NULL
#define _show_message                   _show_message_on_LCD //NULL



// ������ ������
#define BITBAND(addr, bitnum)           ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)                  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)          MEM_ADDR(BITBAND(addr, bitnum)) 

#define GPIOA_ODR_Addr    (GPIOA_BASE+20) //0x40020014
#define GPIOB_ODR_Addr    (GPIOB_BASE+20) //0x40020414 
#define GPIOC_ODR_Addr    (GPIOC_BASE+20) //0x40020814 
#define GPIOD_ODR_Addr    (GPIOD_BASE+20) //0x40020C14 
#define GPIOE_ODR_Addr    (GPIOE_BASE+20) //0x40021014 
#define GPIOF_ODR_Addr    (GPIOF_BASE+20) //0x40021414    
#define GPIOG_ODR_Addr    (GPIOG_BASE+20) //0x40021814   
#define GPIOH_ODR_Addr    (GPIOH_BASE+20) //0x40021C14    
#define GPIOI_ODR_Addr    (GPIOI_BASE+20) //0x40022014     

#define GPIOA_IDR_Addr    (GPIOA_BASE+16) //0x40020010 
#define GPIOB_IDR_Addr    (GPIOB_BASE+16) //0x40020410 
#define GPIOC_IDR_Addr    (GPIOC_BASE+16) //0x40020810 
#define GPIOD_IDR_Addr    (GPIOD_BASE+16) //0x40020C10 
#define GPIOE_IDR_Addr    (GPIOE_BASE+16) //0x40021010 
#define GPIOF_IDR_Addr    (GPIOF_BASE+16) //0x40021410 
#define GPIOG_IDR_Addr    (GPIOG_BASE+16) //0x40021810 
#define GPIOH_IDR_Addr    (GPIOH_BASE+16) //0x40021C10 
#define GPIOI_IDR_Addr    (GPIOI_BASE+16) //0x40022010 
 
//IO???,?????IO?!
//??n????16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //?? 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //?? 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //?? 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //?? 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //?? 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //?? 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //?? 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //?? 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //?? 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //??

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //?? 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //??

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //?? 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //??

#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //?? 
#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //??

#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //?? 
#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //??
   


//#define HAL_EEPROM_SIZE					512 //in bytes
            
// The data for the algorithm


#define BATT_VOLTAGE_MAX                4200


typedef struct EEPROM_config_ { //size must be < 4096 bytes
	char ssid[32]; //��� ����� �������
	char pass[32]; //������ ����� �������


} EEPROM_config;

//WIFI
#define DEFAULT_SSID					"qwerty"
#define DEFAULT_PASS					"11111111"

// end etc. ----------------------------

// post include ------------------------
#include "_fifo.h"


#ifdef	__cplusplus
extern "C" {
#endif
 
uint32_t  millis(void);

#ifdef	__cplusplus
}
#endif

#endif	/* BOARD_H */
