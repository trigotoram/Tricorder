/**
 * @file    drvBMP180.h
 * @author  Ht3h5793, CD45
 * @date    14.09.2015  8.25
 * @version V1.0.0
 * @brief 


�������� � ��������

 */
 

#ifndef DRVBMP180_H
#define	DRVBMP180_H 20150914

#include "board.h"


#ifdef	__cplusplus
extern "C" {
#endif
    

MSG drvBMP180_Calibration (S16 *BMP180_calibration_int16_t, S16 *BMP180_calibration_uint16_t);

//uint16_t bmp180ReadShort(uint8_t address, uint8_t* errorcode);
//S32 bmp180ReadTemp(uint8_t* error_code);
//S32 bmp180ReadPressure(uint8_t* errorcode);

//MSG       drvBMP180_getTemp (S16 *);
//MSG       drvBMP180_getPressure (S32 *);

MSG drvBMP180_Convert (S16 *BMP180_calibration_int16_t, S16 *BMP180_calibration_uint16_t, S32 *temperature, S32 *pressure);
S32 drvBMP180_CalcAltitude (S32 pressure);


#ifdef	__cplusplus
}
#endif

#endif	/** DRVBMP180_H */
