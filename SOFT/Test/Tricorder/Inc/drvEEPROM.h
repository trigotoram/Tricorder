/**
 */
 
/**
  ******************************************************************************
  * @file    EEPROM_Emulation/inc/eeprom.h 
  * @author  MCD Application Team
  * @version V3.1.0
  * @date    07/27/2009
  * @brief   This file contains all the functions prototypes for the EEPROM 
  *          emulation firmware library.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEPROM_H
#define __EEPROM_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "board.h"

/* Exported constants --------------------------------------------------------*/
/* Define the STM32F10Xxx Flash page size depending on the used STM32 device */
#if defined (STM32F10X_LD) || defined (STM32F10X_MD_VL)
    #define PAGE_SIZE  (U16)0x400  /* Page size = 1KByte */
#elif defined (STM32F10X_HD) || defined (STM32F10X_CL)
    #define PAGE_SIZE  (U16)0x800  /* Page size = 2KByte */
#else
    #define PAGE_SIZE   (U16)0x800
//#warning "WATAFAAACJKKK!!!"
#endif

/* EEPROM start address in Flash */
#define EEPROM_START_ADDRESS    ((uint32_t)0x08010000) /* EEPROM emulation start address:
                                                  after 64KByte of used Flash memory */

/* Pages 0 and 1 base and end addresses */
#define PAGE0_BASE_ADDRESS      ((uint32_t)(EEPROM_START_ADDRESS + 0x000))
#define PAGE0_END_ADDRESS       ((uint32_t)(EEPROM_START_ADDRESS + (PAGE_SIZE - 1)))

#define PAGE1_BASE_ADDRESS      ((uint32_t)(EEPROM_START_ADDRESS + PAGE_SIZE))
#define PAGE1_END_ADDRESS       ((uint32_t)(EEPROM_START_ADDRESS + (2 * PAGE_SIZE - 1)))

/* Used Flash pages for EEPROM emulation */
#define PAGE0                   ((U16)0x0000)
#define PAGE1                   ((U16)0x0001)

/* No valid page define */
#define NO_VALID_PAGE           ((U16)0x00AB)

/* Page status definitions */
#define ERASED                  ((U16)0xFFFF)     /* PAGE is empty */
#define RECEIVE_DATA            ((U16)0xEEEE)     /* PAGE is marked to receive data */
#define VALID_PAGE              ((U16)0x0000)     /* PAGE containing valid data */

/* Valid pages in read and write defines */
#define READ_FROM_VALID_PAGE    ((uint8_t)0x00)
#define WRITE_IN_VALID_PAGE     ((uint8_t)0x01)

/* Page full define */
#define PAGE_FULL               ((uint8_t)0x80)

/* Variables' number */
#define NumbOfVar               ((uint8_t)0x9A) //152 ����������

//ZAY_079
#define FLASH_Status            U16
#define FLASH_COMPLETE          2

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
U16 EE_Init(void);
U16 EE_ReadVariable(U16 VirtAddress, U16* Data);
U16 EE_WriteVariable(U16 VirtAddress, U16 Data);

#endif /* __EEPROM_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/

#ifndef DRVEEPROM_H
#define	DRVEEPROM_H 20161205

#include "board.h"


#ifdef	__cplusplus
extern "C" {
#endif
    


//**************��������� ������ � eeprom**************************************************************************//
//������������ �������� ��� ������ 8192 16-�� ��������� �������
void write_EE (U16 adres, U16 data);

//************��������� ������ �� eeprom*****************************************//
U16 read_EE (U16 adress);

//***************��������� ������ ��������*******************//
void drvEEPROM_read_config (void);

#ifdef	__cplusplus
}
#endif

#endif	/** DRVEEPROM_H */
