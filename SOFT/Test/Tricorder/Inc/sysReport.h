/*   
 * @author  Ht3h5793
 * @brief   
    only for EWARM tested
    http://catethysis.ru/predefined-macros/
    
 */

#ifndef SYSREPORT_H
#define	SYSREPORT_H 20170427

#include "board.h"


typedef enum {
    //Errors
	NO_ERROR                            = 0x0,
	ERROR_CMD                           = 0x1,
	ERROR_PARAM                         = 0x10,
	ERROR_STATE                         = 0x20,
	ERROR_OVERHEAT                      = 0x30,
	ERROR_POWER                         = 0x40,
	ERROR_MALFUNCTION                   = 0x50,
	ERROR_INTERNAL                      = 0x60,
    ERROR_UNKNOWN                       = 0x77,
    
    
    ERROR_HAL_COMMON                    = 0x1000,
    ERROR_HAL_PLL_NOT_START             = 0x1001,
    ERROR_HAL_FLASH_LAT                 = 0x1002,
    ERROR_HAL_USB_CLOCK                 = 0x1004,
    
    HAL_TIM_INIT_ERROR                  = 0x1010,
    
    HAL_ADC1_INIT_ERROR                 = 0x1100,
    HAL_ADC2_INIT_ERROR                 = 0x1101,
    HAL_ADC3_INIT_ERROR                 = 0x1102,

    HAL_DAC1_INIT_ERROR                 = 0x1110,
        
    HAL_USART1_ERROR                    = 0x1200,
    HAL_USART2_INIT_ERROR               = 0x1210,
    HAL_USART2_TX_ERROR                 = 0x1211,
    HAL_USART2_RX_ERROR                 = 0x1212,
    HAL_USART2_COM_ERROR                = 0x1213,

   // ERROR_USART3_HAL                    = 0x1202,
   // ERROR_USART4_HAL                    = 0x1203,
    
    HAL_SPI1_INIT_ERROR                 = 0x1300,
    
    FAT_INIT_ERROR                      = 0x2000,
    FAT_REINIT_ERROR                    = 0x2001,
    FAT_READ_FILE_ERROR                 = 0x2002,
    ERROR_FAT_WRITE_FILE                = 0x2005,
    ERROR_FAT_MOUNT_DISK                = 0x2006,
    ERROR_FAT_LSEEK_FILE                = 0x2007,
    ERROR_FAT_CLOSE_FILE                = 0x2008,
    
    ERROR_MODIO_NOT_RESPOND             = 0x3000,
    
    ERROR_RTOS_TASK                     = 0x5000,
    ERROR_RTOS_TASK_NOT_CREATE          = 0x5010,
    ERROR_RTOS_START_SHEDULER           = 0x5020,
    ERROR_RTOS_QUEUE_CREATE             = 0x5100,
    ERROR_RTOS_QUEUE_SEND               = 0x5110,
    
    ERROR_EEPROM_ERASE,
    ERROR_EEPROM_WRITE_PROTECTION,
    ERROR_EEPROM_PROG,

    FIFO_INIT_ERROR                     = 0x6000,
    FIFO_GET_ERROR                      = 0x6001,
    FIFO_PUT_ERROR                      = 0x6002,

    //Warnings
    WARNING_ADC_SHORT2GND               = 0x8001,
    WARNING_ADC_SHORT2VCC               = 0x8002,
    
    
    
} STATUS_ENUM; //ERROR_StatusTypeDef; //ERROR_T


#ifdef	__cplusplus
extern "C" {
#endif
    

void fERROR_ACTION (STATUS_ENUM code, char *module, uint32_t pos);

void fWARNING_ACTION (uint16_t code, char *module, uint32_t pos);

#define ERROR_ACTION(a)                 fERROR_ACTION(a,__FILE__,__LINE__)
#define ERROR_ACTION_STR(a,b)           fERROR_ACTION(b,__FILE__,__LINE__)
#define WARNING_ACTION(a)               fWARNING_ACTION(a,__FILE__,__LINE__)
#define WARNING_ACTION_STR(a,b)         fWARNING_ACTION(b,__FILE__,__LINE__)

#ifdef	__cplusplus
}
#endif

#endif /** SYSREPORT_H */
